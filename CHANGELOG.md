# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased
### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security

## 0.0.3
### Added
- recursive cj-vars expansion
- more cj-vars
- hardcoded defaults for (some) cj-vars
- Permissions
- Prebuild seccomp filters
  - flatpak
  - hide-unknown-syscalls
  - inode-deny-create-device
  - inode-deny-create-suid
  - memory-deny-write-execute
  - namespaces-deny-clone-newuser
- New jailconf options:
  - `syscall-filters.denylist`: Denylist syscalls
  - `syscall-filters.af_allowlist`: Enable allowlisting of address families
  - Enable/Disable prebuild seccomp filters
    - `syscall-filters.flatpak` (default: `true`)
    - `syscall-filters.hide-unknown-syscalls` (default: `true`)
    - `syscall-filters.inode-deny-create-device` (default: `true`)
    - `syscall-filters.inode-deny-create-suid` (default: `true`)
    - `syscall-filters.namespaces-deny-clone-newuser` (default: `true`)
- crabjail-cli: make timestamps in logging optional and add --timestamp

### Changed
- MSRV: 1.60.0
- Tilde expansion is done after env-var expansion
- Further harden the default filesystem
- bubblewrap >= 0.6.0 is now required

### Removed
- jailconf options:
  - `syscall-filters.preset`

## 0.0.2 - 2022-02-27
### Added
- New jailconf options:
  - `devices.private` (default: true): Which can be set to `false` to make all
    devices `/dev` visible by default.
  - ~~`devices.dri` (default: false): Which can be set to `true` to allow dri devices.~~
  - ~~`devices.v4l` (default: false): Which can be set to `true` to allow v4l devices.~~
  - `devices.subsystems` to allow/deny files in `/dev` by subsystem.
  - `filesystem.allow-ro`: allow a path read-only
  - `filesystem.deny`: deny a path
  - `filesystem.mask`: mask a path
  - `filesystem.private`: privately allow a directory
  - `filesystem.tmpfs`: mount a tmpfs at path
  - `filesystem.create-dir`: create a directory at path
  - `syscall-filters.preset` (default: "flatpak"): Select a preset seccomp template.
  - `syscall-filters.memory-deny-write-execute` (default: false): Which can be
    set to `true` for `W^X`.
  - `network.mode` (default: "none"): Set the network mode
    - `"none"`: No network access
    - `"shared"`: Full network access
    - `"private"`: Private network access via slirp4netns (experimental)
- Added `IBUS_USE_PORTAL=1` to the hardcoded environment variables.
- Proxy D-Bus access through xdg-dbus-proxy.
- env-var expansion (`${VARIABLE}`)
- env-var expansion with fallbak (`${VAR:-FALLBACK}`)
- cj-vars expansion
- Minimal D-Bus filtering

### Changed
- MSRV: 1.59.0
- Harden default /sys allowlist
- Harden default /run allowlist
- Deny /sbin and /usr/sbin by default
- Reject unknown keys in jailconfs
- Rename `{session,system}-bus` to `{session,system}-bus.policy`

### Removed

- Removed `GIO_USE_VFS=local` from the hardcoded environment variables.

## 0.0.1 - 2021-11-26
### Added
- WIP documentation based on sphinx
- WIP manpage in reStructuredText
- New jailconf options:
  - `environment.env`: set/unset environment variables
  - `environment.cwd`: set working directory
  - `filesystem.allow`: allow a path
    - tilde expansion is supported
    - unix shell style globbing is supported
  - `filesystem.noallow`: path to ignore when resolving filesystem.allow
- Hardcoded certain environment variables (can be overridden in jailconfs):
  - `container=crabjail`
  - `GSETTINGS_BACKEND=keyfile`
  - `GTK_USE_PORTAL=1`
  - `GIO_USE_VFS=local`
- Set certain bubblewrap arguments (`--unshare-*`, `--new-session`, ...)
  unconditionally
- Mount private `/dev` and `/proc` filesystems

/*
 * Copyright © 2024,2025 The crabjail Authors
 *
 * This file is part of crabjail
 *
 * crabjail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabjail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::borrow::Cow;
use std::fs;
use std::fs::File;
use std::io::prelude::*;
use std::io::{BufWriter, Result as IoResult};
use std::os::unix::prelude::*;
use std::path::PathBuf;
use std::process::Command;
use std::sync::Arc;

use rayon::prelude::*;

#[derive(Debug)]
pub struct CrablockCmd<'a> {
    runtime_dir: Arc<PathBuf>,
    pub(crate) argv: CrablockArgv<'a>,
}
impl CrablockCmd<'_> {
    pub(crate) fn new(runtime_dir: Arc<PathBuf>) -> Self {
        Self {
            runtime_dir,
            argv: CrablockArgv::default(),
        }
    }

    pub fn to_command(&self) -> Command {
        let mut ccmd = Command::new("crablock");
        ccmd.args(self.argv.iter());
        ccmd.arg("--");
        ccmd
    }

    pub fn to_command_args_file(&self) -> IoResult<Command> {
        let args_file_path = self.runtime_dir.join("crablock-argv.txt");

        if let Some(ppath) = args_file_path.parent() {
            fs::create_dir_all(ppath)?;
        }

        let args_file = File::options()
            .write(true)
            .create_new(true)
            .mode(0o600)
            .open(&args_file_path)?;

        let mut f = BufWriter::new(args_file);
        for arg in self.argv.iter() {
            debug_assert!(!arg.contains('\n'));
            f.write_all(arg.as_bytes())?;
            f.write_all(b"\n")?;
        }
        f.flush()?;

        let mut ccmd = Command::new("crablock");
        ccmd.arg("--args-lf");
        ccmd.arg(&args_file_path);
        ccmd.arg("--");
        Ok(ccmd)
    }

    pub fn log_argv(&self) -> &Self {
        if log::max_level() >= log::Level::Debug {
            let argv_string = self
                .argv
                .iter()
                .flat_map(|a| {
                    if a.starts_with("--") {
                        ["\n", a]
                    } else {
                        [" ", a]
                    }
                })
                .collect::<String>();
            log::debug!("crablock arguments:{}", argv_string);
        }

        self
    }
}

#[derive(Debug, Default)]
pub(crate) struct CrablockArgv<'a>(Vec<Cow<'a, str>>);
impl<'a> CrablockArgv<'a> {
    pub(crate) fn append(&mut self, arg: &[Cow<'a, str>]) {
        self.0.extend_from_slice(arg);
    }

    pub(crate) fn extend<I: IntoIterator<Item = Cow<'a, str>>>(&mut self, iter: I) {
        self.0.extend(iter);
    }

    #[expect(dead_code, reason = "Possible future use")]
    pub(crate) fn par_extend<I: IntoParallelIterator<Item = Cow<'a, str>>>(&mut self, iter: I) {
        self.0.par_extend(iter);
    }

    pub(crate) fn reserve(&mut self, additional: usize) {
        self.0.reserve(additional);
    }

    pub(crate) fn iter(&self) -> impl Iterator<Item = &str> {
        self.0.iter().map(|a| &**a)
    }
}

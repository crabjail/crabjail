/*
 * Copyright © 2024,2025 The crabjail Authors
 *
 * This file is part of crabjail
 *
 * crabjail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabjail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use crate::crablock_cmd::CrablockArgv;
use crate::permissions::Namespace;

#[derive(Debug, Clone)]
pub(super) struct NamespacesLimits {
    cgroup: Option<u8>,
    ipc: Option<u8>,
    mnt: Option<u8>,
    net: Option<u8>,
    pid: Option<u8>,
    time: Option<u8>,
    user: Option<u8>,
    uts: Option<u8>,
}
impl NamespacesLimits {
    pub(super) fn set_limit(&mut self, namespace: Namespace, limit: Option<u8>) {
        let self_ns = match namespace {
            Namespace::Cgroup => &mut self.cgroup,
            Namespace::Ipc => &mut self.ipc,
            Namespace::Mnt => &mut self.mnt,
            Namespace::Net => &mut self.net,
            Namespace::Pid => &mut self.pid,
            Namespace::Time => &mut self.time,
            Namespace::User => &mut self.user,
            Namespace::Uts => &mut self.uts,
        };

        if let Some(new_count) = limit {
            if let Some(cur_count) = *self_ns {
                if new_count > cur_count {
                    *self_ns = limit;
                }
            }
        } else {
            *self_ns = limit;
        }
    }

    pub(super) fn to_crablock_argv(&self, argv: &mut CrablockArgv<'_>) {
        let namespaces_limits = [
            ("--max-cgroup-namespaces", self.cgroup),
            ("--max-ipc-namespaces", self.ipc),
            ("--max-mnt-namespaces", self.mnt),
            ("--max-net-namespaces", self.net),
            ("--max-pid-namespaces", self.pid),
            ("--max-time-namespaces", self.time),
            ("--max-user-namespaces", self.user),
            ("--max-uts-namespaces", self.uts),
        ];
        for (arg, limit) in namespaces_limits {
            if let Some(count) = limit {
                argv.append(&clock_arg![
                    arg,
                    itoa::Buffer::new().format(count).to_string()
                ]);
            }
        }
        if self.user.is_some_and(|count| count == 0) {
            argv.append(&clock_arg!["--seccomp-deny-clone-newuser"]);
        }
    }
}
impl Default for NamespacesLimits {
    fn default() -> Self {
        Self {
            cgroup: Some(0),
            ipc: Some(0),
            mnt: Some(0),
            net: Some(0),
            pid: Some(0),
            time: Some(0),
            user: Some(0),
            uts: Some(0),
        }
    }
}

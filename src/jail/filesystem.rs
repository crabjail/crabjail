/*
 * Copyright © 2024,2025 The crabjail Authors
 *
 * This file is part of crabjail
 *
 * crabjail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabjail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::collections::HashMap;
use std::collections::hash_map::Entry;
use std::path::PathBuf;

use crate::crablock_cmd::CrablockArgv;
use crate::permissions::{FsAccess, FsAction, FsPolicy};

#[derive(Debug, Default, Clone)]
pub(super) struct Filesystem {
    paths: HashMap<PathBuf, FsPolicy>,
    full_proc: bool,
}
impl Filesystem {
    pub(super) fn add<P: Into<PathBuf>>(&mut self, path: P, action: FsAction, access: FsAccess) {
        match self.paths.entry(path.into()) {
            Entry::Occupied(mut e) => {
                if e.get().action != action {
                    match (&e.get().action, &action) {
                        (_, FsAction::Access) => (),
                        (FsAction::Access, _) => e.get_mut().action = action,
                        (cur_act, new_act) => {
                            unimplemented!(
                                "Tried to change {cur_act:?} to {new_act:?} on {:?}",
                                e.key()
                            )
                        }
                    }
                }
                e.get_mut().access |= access;
            }
            Entry::Vacant(e) => {
                e.insert(FsPolicy {
                    action: action.clone(),
                    access,
                });
            }
        }
    }

    pub(super) fn set_full_proc(&mut self) {
        self.full_proc = true;
    }

    pub(super) fn to_crablock_argv<'a>(&'a self, argv: &mut CrablockArgv<'a>) {
        // We do not know the number of arguments before but it will be lot.
        // Five times the number of paths is likely a good average choice given
        // the most paths will have an action (usually 3 arguments) and at least
        // one access (at least 2 arguments).
        argv.reserve(self.paths.len() * 5);

        if self.full_proc {
            argv.append(&clock_arg!["--mount-proc-subset", "none"]);
        } else {
            argv.append(&clock_arg!["--mount-proc-subset", "pid"]);
        }

        argv.append(
            [
                clock_arg!["--mnt-mask", "/"],
                clock_arg!["--mnt-ro", "/"],
                clock_arg!["--mnt-nosuid", "/"],
                clock_arg!["--mnt-nodev", "/"],
                clock_arg!["--mnt-noexec", "/"],
            ]
            .as_flattened(),
        );

        for (path, policy) in &self.paths {
            let path = path.to_str().expect("utf-8");
            let mut flags = "+e";

            match policy.action {
                FsAction::Allow => {
                    argv.append(&clock_arg!["--mnt-allow", flags, path]);
                    policy.access.to_crablock_argv(argv, flags, path);
                }
                FsAction::AllowSymlinkTarget => {
                    flags = "+ef";
                    argv.append(&clock_arg!["--mnt-allow", flags, path]);
                    policy.access.to_crablock_argv(argv, flags, path);
                }
                FsAction::AllowAndSymlinkTarget => {
                    argv.append(&clock_arg!["--mnt-allow", flags, path]);
                    policy.access.to_crablock_argv(argv, flags, path);
                    flags = "+ef";
                    argv.append(&clock_arg!["--mnt-allow", flags, path]);
                    policy.access.to_crablock_argv(argv, flags, path);
                }
                FsAction::Private => {
                    argv.append(&clock_arg!["--mnt-private", flags, path]);
                    policy.access.to_crablock_argv(argv, flags, path);
                }
                FsAction::PrivateVolatile => {
                    argv.append(&clock_arg!["--mnt-private-volatile", flags, path]);
                    policy.access.to_crablock_argv(argv, flags, path);
                }
                FsAction::Access => {
                    policy.access.to_crablock_argv(argv, flags, path);
                }
                FsAction::_MaskDev => {
                    argv.append(&clock_arg!["--mnt-mask-dev", flags, path]);
                    policy.access.to_crablock_argv(argv, flags, path);
                }
            }
        }
    }
}

impl FsAccess {
    // const operators are unstable
    const LANDLOCK_FS_CUSTOM: Self = Self::empty()
        .union(Self::LANDLOCK_EXECUTE)
        .union(Self::LANDLOCK_WRITE_FILE)
        .union(Self::LANDLOCK_READ_FILE)
        .union(Self::LANDLOCK_READ_DIR)
        .union(Self::LANDLOCK_REMOVE_DIR)
        .union(Self::LANDLOCK_REMOVE_FILE)
        .union(Self::LANDLOCK_MAKE_CHAR)
        .union(Self::LANDLOCK_MAKE_DIR)
        .union(Self::LANDLOCK_MAKE_REG)
        .union(Self::LANDLOCK_MAKE_SOCK)
        .union(Self::LANDLOCK_MAKE_FIFO)
        .union(Self::LANDLOCK_MAKE_BLOCK)
        .union(Self::LANDLOCK_MAKE_SYM)
        .union(Self::LANDLOCK_REFER)
        .union(Self::LANDLOCK_MAYBE_REFER)
        .union(Self::LANDLOCK_TRUNCATE)
        .union(Self::LANDLOCK_MAYBE_TRUNCATE)
        .union(Self::LANDLOCK_IOCTL_DEV)
        .union(Self::LANDLOCK_MAYBE_IOCTL_DEV);

    fn to_crablock_argv<'a>(self, argv: &mut CrablockArgv<'a>, flags: &'a str, path: &'a str) {
        if self.contains(Self::READ) {
            argv.append(&clock_arg!["--landlock-fs-readable-path", path]);
        }
        if self.contains(Self::WRITE) {
            argv.append(&clock_arg!["--landlock-fs-writable-path", path]);
            argv.append(&clock_arg!["--mnt-rw", flags, path]);
            // FIXME Make this a crablock default
            // if path == "/proc" {
            //     argv.append(&clock_arg!["--mount-proc-read-write"]);
            // }
        }
        if self.contains(Self::LIST) {
            argv.append(&clock_arg!["--landlock-fs-listable-path", path]);
        }
        if self.contains(Self::MANAGE) {
            argv.append(&clock_arg!["--landlock-fs-manageable-path", path]);
            argv.append(&clock_arg!["--mnt-rw", flags, path]);
        }
        if self.contains(Self::EXECUTE) {
            argv.append(&clock_arg!["--landlock-fs-executable-path", path]);
            argv.append(&clock_arg!["--mnt-exec", flags, path]);
        }
        if self.contains(Self::EXECUTE_SUID) {
            argv.append(&clock_arg!["--landlock-fs-executable-path", path]);
            argv.append(&clock_arg!["--mnt-suid", flags, path]);
            argv.append(&clock_arg!["--mnt-exec", flags, path]);
        }
        if self.contains(Self::ACCESS_DEVICES) {
            argv.append(&clock_arg!["--mnt-dev", flags, path]);
        }
        if self.intersects(Self::LANDLOCK_FS_CUSTOM) {
            if self.contains(Self::LANDLOCK_EXECUTE) {
                argv.append(&clock_arg!["--landlock-fs-custom", "EXECUTE", path]);
            }
            if self.contains(Self::LANDLOCK_WRITE_FILE) {
                argv.append(&clock_arg!["--landlock-fs-custom", "WRITE_FILE", path]);
            }
            if self.contains(Self::LANDLOCK_READ_FILE) {
                argv.append(&clock_arg!["--landlock-fs-custom", "READ_FILE", path]);
            }
            if self.contains(Self::LANDLOCK_READ_DIR) {
                argv.append(&clock_arg!["--landlock-fs-custom", "READ_DIR", path]);
            }
            if self.contains(Self::LANDLOCK_REMOVE_DIR) {
                argv.append(&clock_arg!["--landlock-fs-custom", "REMOVE_DIR", path]);
            }
            if self.contains(Self::LANDLOCK_REMOVE_FILE) {
                argv.append(&clock_arg!["--landlock-fs-custom", "REMOVE_FILE", path]);
            }
            if self.contains(Self::LANDLOCK_MAKE_CHAR) {
                argv.append(&clock_arg!["--landlock-fs-custom", "MAKE_CHAR", path]);
            }
            if self.contains(Self::LANDLOCK_MAKE_DIR) {
                argv.append(&clock_arg!["--landlock-fs-custom", "MAKE_DIR", path]);
            }
            if self.contains(Self::LANDLOCK_MAKE_REG) {
                argv.append(&clock_arg!["--landlock-fs-custom", "MAKE_REG", path]);
            }
            if self.contains(Self::LANDLOCK_MAKE_SOCK) {
                argv.append(&clock_arg!["--landlock-fs-custom", "MAKE_SOCK", path]);
            }
            if self.contains(Self::LANDLOCK_MAKE_FIFO) {
                argv.append(&clock_arg!["--landlock-fs-custom", "MAKE_FIFO", path]);
            }
            if self.contains(Self::LANDLOCK_MAKE_BLOCK) {
                argv.append(&clock_arg!["--landlock-fs-custom", "MAKE_BLOCK", path]);
            }
            if self.contains(Self::LANDLOCK_MAKE_SYM) {
                argv.append(&clock_arg!["--landlock-fs-custom", "MAKE_SYM", path]);
            }
            if self.contains(Self::LANDLOCK_REFER) {
                argv.append(&clock_arg!["--landlock-fs-custom", "REFER", path]);
            }
            if self.contains(Self::LANDLOCK_MAYBE_REFER) {
                argv.append(&clock_arg!["--landlock-fs-custom", "REFER?", path]);
            }
            if self.contains(Self::LANDLOCK_TRUNCATE) {
                argv.append(&clock_arg!["--landlock-fs-custom", "TRUNCATE", path]);
            }
            if self.contains(Self::LANDLOCK_MAYBE_TRUNCATE) {
                argv.append(&clock_arg!["--landlock-fs-custom", "TRUNCATE?", path]);
            }
            if self.contains(Self::LANDLOCK_IOCTL_DEV) {
                argv.append(&clock_arg!["--landlock-fs-custom", "IOCTL_DEV", path]);
            }
            if self.contains(Self::LANDLOCK_MAYBE_IOCTL_DEV) {
                argv.append(&clock_arg!["--landlock-fs-custom", "IOCTL_DEV?", path]);
            }
        }
        if self.contains(Self::UNRESTRICTED) {
            argv.append(&clock_arg!["--landlock-fs-unrestricted-path", path]);
            argv.append(&clock_arg!["--mnt-suid", flags, path]);
            argv.append(&clock_arg!["--mnt-dev", flags, path]);
            argv.append(&clock_arg!["--mnt-exec", flags, path]);
        }
    }
}

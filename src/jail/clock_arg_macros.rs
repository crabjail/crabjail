/*
 * Copyright © 2024,2025 The crabjail Authors
 *
 * This file is part of crabjail
 *
 * crabjail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabjail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#[rustfmt::skip]
macro_rules! _clock_arg_option0 {
    ("--no-new-privs") => { "--no-new-privs" };
    ("--mount-devpts") => { "--mount-devpts" };
    ("--mount-mqueue") => { "--mount-mqueue" };
    ("--mount-proc") => { "--mount-proc" };
    ("--mount-proc-read-only") => { "--mount-proc-read-only" };
    ("--mount-proc-read-write") => { "--mount-proc-read-write" };
    ("--pasta") => { "--pasta" };
    ("--landlock-scope-abstract-unix-socket") => { "--landlock-scope-abstract-unix-socket" };
    ("--landlock-scope-signal") => { "--landlock-scope-signal" };
    ("--seccomp-flatpak") => { "--seccomp-flatpak" };
    ("--seccomp-memfd-noexec") => { "--seccomp-memfd-noexec" };
    ("--seccomp-deny-memory-write-execute") => { "--seccomp-deny-memory-write-execute" };
    ("--seccomp-deny-clone-newuser") => { "--seccomp-deny-clone-newuser" };
    ("--seccomp-deny-execve-null") => { "--seccomp-deny-execve-null" };
    ("--clearenv") => { "--clearenv" };
    ("--mdwe-refuse-exec-gain") => { "--mdwe-refuse-exec-gain" };
    ("--new-session") => { "--new-session" };
    ("--strict-mitigations") => { "--strict-mitigations" };
    ("--custom-init") => { "--custom-init" };
    ("--die-with-parent") => { "--die-with-parent" };
}

#[rustfmt::skip]
macro_rules! _clock_arg_option1 {
    ("--args-lf") => { "--args-lf" };
    ("--args-nul") => { "--args-nul" };
    ("--sandbox-name") => { "--sandbox-name" };
    ("--unshare") => { "--unshare" };
    ("--uid") => { "--uid" };
    ("--gid") => { "--gid" };
    ("--map-uids") => { "--map-uids" };
    ("--map-gids") => { "--map-gids" };
    ("--setuid") => { "--setuid" };
    ("--setgid") => { "--setgid" };
    ("--capabilities") => { "--capabilities" };
    ("--securebits") => { "--securebits" };
    ("--mount-proc-subset") => { "--mount-proc-subset" };
    ("--fsm-umount") => { "--fsm-umount" };
    ("--fsm-chdir") => { "--fsm-chdir" };
    ("--fsm-chroot") => { "--fsm-chroot" };
    ("--mnt-mask") => { "--mnt-mask" };
    ("--mnt-mask-dev") => { "--mnt-mask-dev" };
    ("--mnt-private-volatile") => { "--mnt-private-volatile" };
    ("--mnt-private") => { "--mnt-private" };
    ("--mnt-allow-volatile") => { "--mnt-allow-volatile" };
    ("--mnt-allow") => { "--mnt-allow" };
    ("--mnt-deny") => { "--mnt-deny" };
    ("--mnt-ro") => { "--mnt-ro" };
    ("--mnt-rw") => { "--mnt-rw" };
    ("--mnt-nosuid") => { "--mnt-nosuid" };
    ("--mnt-suid") => { "--mnt-suid" };
    ("--mnt-nodev") => { "--mnt-nodev" };
    ("--mnt-dev") => { "--mnt-dev" };
    ("--mnt-noexec") => { "--mnt-noexec" };
    ("--mnt-exec") => { "--mnt-exec" };
    ("--cwd") => { "--cwd" };
    ("--mount") => { "--mount" };
    ("--hostname") => { "--hostname" };
    ("--landlock-fs-executable-path") => { "--landlock-fs-executable-path" };
    ("--landlock-fs-readable-path") => { "--landlock-fs-readable-path" };
    ("--landlock-fs-writable-path") => { "--landlock-fs-writable-path" };
    ("--landlock-fs-listable-path") => { "--landlock-fs-listable-path" };
    ("--landlock-fs-manageable-path") => { "--landlock-fs-manageable-path" };
    ("--landlock-fs-device-useable-path") => { "--landlock-fs-device-useable-path" };
    ("--landlock-fs-unrestricted-path") => { "--landlock-fs-unrestricted-path" };
    ("--landlock-restrict-bind-tcp") => { "--landlock-restrict-bind-tcp" };
    ("--landlock-restrict-connect-tcp") => { "--landlock-restrict-connect-tcp" };
    ("--add-seccomp-fd") => { "--add-seccomp-fd" };
    ("--seccomp-syscall-filter") => { "--seccomp-syscall-filter" };
    ("--seccomp-restrict-iotl") => { "--seccomp-restrict-iotl" };
    ("--seccomp-restrict-prctl") => { "--seccomp-restrict-prctl" };
    ("--seccomp-restrict-socket") => { "--seccomp-restrict-socket" };
    ("--max-cgroup-namespaces") => { "--max-cgroup-namespaces" };
    ("--max-ipc-namespaces") => { "--max-ipc-namespaces" };
    ("--max-mnt-namespaces") => { "--max-mnt-namespaces" };
    ("--max-net-namespaces") => { "--max-net-namespaces" };
    ("--max-pid-namespaces") => { "--max-pid-namespaces" };
    ("--max-time-namespaces") => { "--max-time-namespaces" };
    ("--max-user-namespaces") => { "--max-user-namespaces" };
    ("--max-uts-namespaces") => { "--max-uts-namespaces" };
    ("--choom") => { "--choom" };
    ("--nice") => { "--nice" };
    ("--keepenv") => { "--keepenv" };
    ("--unsetenv") => { "--unsetenv" };
    ("--setenv") => { "--setenv" };
    ("--umask") => { "--umask" };
    ("--crablock-data-home") => { "--crablock-data-home" };
    ("--crablock-runtime-dir") => { "--crablock-runtime-dir" };
    ("--dbus-proxy-capture-output") => { "--dbus-proxy-capture-output" };
}

#[rustfmt::skip]
macro_rules! _clock_arg_option2 {
    ("--fsm-bind") => { "--fsm-bind" };
    ("--fsm-rbind") => { "--fsm-rbind" };
    ("--fsm-move-mount") => { "--fsm-move-mount" };
    ("--fsm-pivot-root") => { "--fsm-pivot-root" };
    ("--fsm-mount-setattr") => { "--fsm-mount-setattr" };
    ("--mnt-mask") => { "--mnt-mask" };
    ("--mnt-mask-dev") => { "--mnt-mask-dev" };
    ("--mnt-private-volatile") => { "--mnt-private-volatile" };
    ("--mnt-private") => { "--mnt-private" };
    ("--mnt-allow-volatile-from") => { "--mnt-allow-volatile-from" };
    ("--mnt-allow-volatile") => { "--mnt-allow-volatile" };
    ("--mnt-allow-from") => { "--mnt-allow-from" };
    ("--mnt-allow") => { "--mnt-allow" };
    ("--mnt-deny") => { "--mnt-deny" };
    ("--mnt-ro") => { "--mnt-ro" };
    ("--mnt-rw") => { "--mnt-rw" };
    ("--mnt-nosuid") => { "--mnt-nosuid" };
    ("--mnt-suid") => { "--mnt-suid" };
    ("--mnt-nodev") => { "--mnt-nodev" };
    ("--mnt-dev") => { "--mnt-dev" };
    ("--mnt-noexec") => { "--mnt-noexec" };
    ("--mnt-exec") => { "--mnt-exec" };
    ("--landlock-fs-custom") => { "--landlock-fs-custom" };
    ("--dbus-proxy") => { "--dbus-proxy" };
}

#[rustfmt::skip]
macro_rules! _clock_arg_option3 {
    ("--mnt-allow-volatile-from") => { "--mnt-allow-volatile-from" };
    ("--mnt-allow-from") => { "--mnt-allow-from" };
    ("--dbus-proxy") => { "--dbus-proxy" };
}

#[rustfmt::skip]
macro_rules! _clock_arg_option4 {
    ("--fsm-mount") => { "--fsm-mount" };
}

macro_rules! clock_arg {
    // Specialcase to simplify code in NamespacesLimits.
    [$opt:ident, $val1:expr $(,)?] => {{
        debug_assert!([
            "--max-cgroup-namespaces",
            "--max-ipc-namespaces",
            "--max-mnt-namespaces",
            "--max-net-namespaces",
            "--max-pid-namespaces",
            "--max-time-namespaces",
            "--max-user-namespaces",
            "--max-uts-namespaces"
        ]
        .contains(&$opt));

        [
            ::std::borrow::Cow::from($opt),
            ::std::borrow::Cow::from($val1),
        ]
    }};
    [$opt:tt $(,)?] => {
        [
            ::std::borrow::Cow::from(_clock_arg_option0!($opt)),
        ]
    };
    [$opt:tt, $val1:expr $(,)?] => {
        [
             ::std::borrow::Cow::from(_clock_arg_option1!($opt)),
             ::std::borrow::Cow::from($val1),
        ]
    };
    [$opt:tt, $val1:expr, $val2:expr $(,)?] => {
        [
             ::std::borrow::Cow::from(_clock_arg_option2!($opt)),
             ::std::borrow::Cow::from($val1),
             ::std::borrow::Cow::from($val2),
        ]
    };
    [$opt:tt, $val1:expr, $val2:expr, $val3:expr $(,)?] => {
        [
             ::std::borrow::Cow::from(_clock_arg_option3!($opt)),
             ::std::borrow::Cow::from($val1),
             ::std::borrow::Cow::from($val2),
             ::std::borrow::Cow::from($val3),
        ]
    };
    [$opt:tt, $val1:expr, $val2:expr, $val3:expr, $val4:expr $(,)?] => {
        [
             ::std::borrow::Cow::from(_clock_arg_option4!($opt)),
             ::std::borrow::Cow::from($val1),
             ::std::borrow::Cow::from($val2),
             ::std::borrow::Cow::from($val3),
             ::std::borrow::Cow::from($val4),
        ]
    };
}

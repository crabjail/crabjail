/*
 * Copyright © 2024,2025 The crabjail Authors
 *
 * This file is part of crabjail
 *
 * crabjail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabjail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::collections::HashSet;

use bitflags::bitflags;

use crate::crablock_cmd::CrablockArgv;
use crate::permissions::{Connectivity, Socket};

#[derive(Debug, Default, Clone)]
pub(super) struct Network {
    bind_tcp: HashSet<u16>,
    bind_tcp_all: bool,
    connect_tcp: HashSet<u16>,
    connect_tcp_all: bool,
    connectivity: Connectivity,
    sockets: Sockets,
}
impl Network {
    pub(super) fn add_bind_tcp(&mut self, port: u16) {
        self.bind_tcp.insert(port);
    }

    pub(super) fn set_bind_tcp_all(&mut self) {
        self.bind_tcp_all = true;
    }

    pub(super) fn add_connect_tcp(&mut self, port: u16) {
        self.connect_tcp.insert(port);
    }

    pub(super) fn set_connect_tcp_all(&mut self) {
        self.connect_tcp_all = true;
    }

    pub(super) fn set_connectivity(&mut self, connectivity: Connectivity) {
        if self.connectivity != connectivity {
            if self.connectivity == Connectivity::None {
                self.connectivity = connectivity;
            } else {
                unimplemented!();
            }
        }
    }

    pub(super) fn add_socket(&mut self, socket: Socket) {
        self.sockets.insert(Sockets::from(socket));
    }

    pub(super) fn to_crablock_argv(&self, argv: &mut CrablockArgv<'_>) {
        if !self.bind_tcp_all {
            if self.bind_tcp.is_empty() {
                argv.append(&clock_arg!["--landlock-restrict-bind-tcp", "none"]);
            } else {
                let mut buffer = itoa::Buffer::new();
                argv.append(&clock_arg![
                    "--landlock-restrict-bind-tcp",
                    self.bind_tcp
                        .iter()
                        .copied()
                        // iter_intersperse is unstable
                        .fold(String::new(), |ports, port| ports
                            + buffer.format(port)
                            + ",",)
                ]);
            }
        }

        if !self.connect_tcp_all {
            if self.connect_tcp.is_empty() {
                argv.append(&clock_arg!["--landlock-restrict-connect-tcp", "none"]);
            } else {
                let mut buffer = itoa::Buffer::new();
                argv.append(&clock_arg![
                    "--landlock-restrict-connect-tcp",
                    self.connect_tcp
                        .iter()
                        .copied()
                        // iter_intersperse is unstable
                        .fold(String::new(), |ports, port| ports
                            + buffer.format(port)
                            + ",")
                ]);
            }
        }

        match self.connectivity {
            Connectivity::None => (),
            Connectivity::Pasta => argv.append(&clock_arg!["--pasta"]),
            Connectivity::Host => argv.append(&clock_arg!["--unshare", "-net"]),
            Connectivity::Tor => unimplemented!(),
        }

        if self.sockets.is_empty() {
            argv.append(&clock_arg!["--seccomp-restrict-socket", "none"]);
        } else {
            argv.append(&clock_arg![
                "--seccomp-restrict-socket",
                self.sockets.to_crablock()
            ]);
        }
    }
}

bitflags! {
    #[derive(Debug, Default, Clone, Copy)]
    struct Sockets: u16 {
        const UNIX = 1 << 0;
        const IP = Self::IP4.bits() | Self::IP6.bits();
        const IP_TCP = Self::IP4_TCP.bits() | Self::IP6_TCP.bits();
        const IP_UDP = Self::IP4_UDP.bits() | Self::IP6_UDP.bits();
        const IP4 = Self::IP4_TCP.bits() | Self::IP4_UDP.bits();
        const IP4_TCP = 1 << 1;
        const IP4_UDP = 1 << 2;
        const IP6 = Self::IP6_TCP.bits() | Self::IP6_UDP.bits();
        const IP6_TCP = 1 << 3;
        const IP6_UDP = 1 << 4;
        const NETLINK = 1 << 5;
        const NETLINK_GENERIC = 1 << 6;
        const NETLINK_ROUTE = 1 << 7;
        const NETLINK_SOCK_DIAG = 1 << 8;
    }
}
impl Sockets {
    fn to_crablock(self) -> String {
        let mut sockets = String::new();
        if self.contains(Self::UNIX) {
            sockets.push_str(concat!(
                "AF_UNIX,SOCK_STREAM,0;",
                "AF_UNIX,SOCK_DGRAM,0;",
                "AF_UNIX,SOCK_SEQPACKET,0;",
            ));
        }
        if self.contains(Self::IP4_TCP) {
            sockets.push_str(concat!(
                "AF_INET,SOCK_STREAM,IPPROTO_IP;",
                "AF_INET,SOCK_STREAM,IPPROTO_TCP;",
            ));
        }
        if self.contains(Self::IP4_UDP) {
            sockets.push_str(concat!(
                "AF_INET,SOCK_DGRAM,IPPROTO_IP;",
                "AF_INET,SOCK_DGRAM,IPPROTO_UDP;",
            ));
        }
        if self.contains(Self::IP6_TCP) {
            sockets.push_str(concat!(
                "AF_INET6,SOCK_STREAM,IPPROTO_IP;",
                "AF_INET6,SOCK_STREAM,IPPROTO_TCP;",
            ));
        }
        if self.contains(Self::IP6_UDP) {
            sockets.push_str(concat!(
                "AF_INET6,SOCK_DGRAM,IPPROTO_IP;",
                "AF_INET6,SOCK_DGRAM,IPPROTO_UDP;",
            ));
        }
        if self.contains(Self::NETLINK) {
            sockets.push_str(concat!("AF_NETLINK,SOCK_DGRAM,;", "AF_NETLINK,SOCK_RAW,;",));
        } else {
            if self.contains(Self::NETLINK_GENERIC) {
                sockets.push_str(concat!(
                    "AF_NETLINK,SOCK_DGRAM,NETLINK_GENERIC;",
                    "AF_NETLINK,SOCK_RAW,NETLINK_GENERIC;",
                ));
            }
            if self.contains(Self::NETLINK_ROUTE) {
                sockets.push_str(concat!(
                    "AF_NETLINK,SOCK_DGRAM,NETLINK_ROUTE;",
                    "AF_NETLINK,SOCK_RAW,NETLINK_ROUTE;",
                ));
            }
            if self.contains(Self::NETLINK_SOCK_DIAG) {
                sockets.push_str(concat!(
                    "AF_NETLINK,SOCK_DGRAM,NETLINK_SOCK_DIAG;",
                    "AF_NETLINK,SOCK_RAW,NETLINK_SOCK_DIAG;",
                ));
            }
        }
        sockets
    }
}
impl From<Socket> for Sockets {
    fn from(socket: Socket) -> Self {
        match socket {
            Socket::Unix => Self::UNIX,
            Socket::Ip => Self::IP,
            Socket::IpTcp => Self::IP_TCP,
            Socket::IpUdp => Self::IP_UDP,
            Socket::Ip4 => Self::IP4,
            Socket::Ip4Tcp => Self::IP4_TCP,
            Socket::Ip4Udp => Self::IP4_UDP,
            Socket::Ip6 => Self::IP6,
            Socket::Ip6Tcp => Self::IP6_TCP,
            Socket::Ip6Udp => Self::IP6_UDP,
            Socket::Netlink => Self::NETLINK,
            Socket::NetlinkGeneric => Self::NETLINK_GENERIC,
            Socket::NetlinkRoute => Self::NETLINK_ROUTE,
            Socket::NetlinkSockDiag => Self::NETLINK_SOCK_DIAG,
        }
    }
}

/*
 * Copyright © 2024,2025 The crabjail Authors
 *
 * This file is part of crabjail
 *
 * crabjail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabjail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::collections::HashMap;
use std::collections::hash_map::Entry;
use std::path::Path;

use lnix::std_suppl::prelude::*;

use crate::crablock_cmd::CrablockArgv;
use crate::permissions::DBusFilterRule;

#[derive(Debug, Default, Clone)]
pub(super) struct SessionBusProxy(DBusProxy);
impl SessionBusProxy {
    pub(super) fn own_name<S: Into<String>>(&mut self, name: S) {
        self.0.own_name(name.into());
    }

    pub(super) fn talk_name<S: Into<String>>(&mut self, name: S) {
        self.0.talk_name(name.into());
    }

    pub(super) fn talk_name_restricted<S: Into<String>>(&mut self, name: S, rule: DBusFilterRule) {
        self.0.talk_name_restricted(name.into(), rule);
    }

    pub(super) fn see_name<S: Into<String>>(&mut self, name: S) {
        self.0.see_name(name.into());
    }

    pub(super) fn to_crablock_argv(
        &self,
        argv: &mut CrablockArgv<'_>,
        jail_instance_runtime_dir: &Path,
    ) {
        self.0.to_crablock_argv(
            argv,
            jail_instance_runtime_dir,
            "session-bus",
            "DBUS_SESSION_BUS_ADDRESS",
        );
    }
}

#[derive(Debug, Default, Clone)]
pub(super) struct SystemBusProxy(DBusProxy);
impl SystemBusProxy {
    pub(super) fn own_name<S: Into<String>>(&mut self, name: S) {
        self.0.own_name(name.into());
    }

    pub(super) fn talk_name<S: Into<String>>(&mut self, name: S) {
        self.0.talk_name(name.into());
    }

    pub(super) fn talk_name_restricted<S: Into<String>>(&mut self, name: S, rule: DBusFilterRule) {
        self.0.talk_name_restricted(name.into(), rule);
    }

    pub(super) fn see_name<S: Into<String>>(&mut self, name: S) {
        self.0.see_name(name.into());
    }

    pub(super) fn to_crablock_argv(
        &self,
        argv: &mut CrablockArgv<'_>,
        jail_instance_runtime_dir: &Path,
    ) {
        self.0.to_crablock_argv(
            argv,
            jail_instance_runtime_dir,
            "system-bus",
            "DBUS_SYSTEM_BUS_ADDRESS",
        );
    }
}

#[derive(Debug, Default, Clone)]
struct DBusProxy {
    names: HashMap<String, Policy>,
}
impl DBusProxy {
    fn own_name(&mut self, name: String) {
        match self.names.entry(name) {
            Entry::Vacant(ent) => {
                ent.insert(Policy::Own);
            }
            Entry::Occupied(mut ent) => {
                ent.insert(Policy::Own);
            }
        }
    }

    fn talk_name(&mut self, name: String) {
        match self.names.entry(name) {
            Entry::Vacant(ent) => {
                ent.insert(Policy::Talk(None));
            }
            Entry::Occupied(mut ent) => {
                if *ent.get() == Policy::Own {
                    unimplemented!();
                }
                ent.insert(Policy::Talk(None));
            }
        }
    }

    fn talk_name_restricted(&mut self, name: String, rule: DBusFilterRule) {
        match self.names.entry(name) {
            Entry::Vacant(ent) => {
                ent.insert(Policy::Talk(Some(vec![rule])));
            }
            Entry::Occupied(mut ent) => match ent.get_mut() {
                Policy::Own | Policy::Talk(None) => {
                    unimplemented!();
                }
                Policy::Talk(Some(rules)) => {
                    rules.push(rule);
                }
                Policy::See => {
                    ent.insert(Policy::Talk(Some(vec![rule])));
                }
            },
        }
    }

    fn see_name(&mut self, name: String) {
        match self.names.entry(name) {
            Entry::Vacant(ent) => {
                ent.insert(Policy::See);
            }
            Entry::Occupied(ent) => {
                if *ent.get() != Policy::See {
                    unimplemented!();
                }
            }
        }
    }

    fn to_crablock_argv<'a>(
        &self,
        argv: &mut CrablockArgv<'a>,
        jail_instance_runtime_dir: &Path,
        bus_name: &'a str,
        dbus_address_var: &str,
    ) {
        let bus_path = jail_instance_runtime_dir
            .join(bus_name)
            .into_string()
            .expect("utf-8");

        argv.append(&clock_arg!["--mnt-allow", "+n", bus_path.clone()]);
        argv.append(&clock_arg![
            "--setenv",
            format!("{dbus_address_var}=unix:path={bus_path}"),
        ]);
        argv.append(&clock_arg![
            "--dbus-proxy",
            bus_name,
            format!("@{{{dbus_address_var}}}"),
            bus_path,
        ]);

        argv.reserve(self.names.len());
        for (name, policy) in &self.names {
            match policy {
                Policy::Own => {
                    argv.append(&clock_arg![
                        "--dbus-proxy",
                        bus_name,
                        format!("--own={name}")
                    ]);
                }
                Policy::Talk(None) => {
                    argv.append(&clock_arg![
                        "--dbus-proxy",
                        bus_name,
                        format!("--talk={name}")
                    ]);
                }
                Policy::Talk(Some(rules)) => {
                    let mut arg = String::new();
                    for rule in rules {
                        if rule.call {
                            arg.push_str("--call=");
                            arg.push_str(name);
                            arg.push('=');
                            if let Some(method) = &rule.method {
                                arg.push_str(method);
                            }
                            if let Some(path) = &rule.path {
                                arg.push('@');
                                arg.push_str(path);
                            }
                            argv.append(&clock_arg!["--dbus-proxy", bus_name, arg.clone()]);
                            arg.clear();
                        }
                        if rule.broadcast {
                            arg.push_str("--broadcast=");
                            arg.push_str(name);
                            arg.push('=');
                            if let Some(method) = &rule.method {
                                arg.push_str(method);
                            }
                            if let Some(path) = &rule.path {
                                arg.push('@');
                                arg.push_str(path);
                            }
                            argv.append(&clock_arg!["--dbus-proxy", bus_name, arg.clone()]);
                            arg.clear();
                        }
                    }
                }
                Policy::See => {
                    argv.append(&clock_arg![
                        "--dbus-proxy",
                        bus_name,
                        format!("--see={name}")
                    ]);
                }
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
enum Policy {
    Own,
    Talk(Option<Vec<DBusFilterRule>>),
    See,
}

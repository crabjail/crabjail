/*
 * Copyright © 2024,2025 The crabjail Authors
 *
 * This file is part of crabjail
 *
 * crabjail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabjail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::collections::HashSet;

use compact_str::{CompactString, ToCompactString};

use crate::config::syscall_groups;
use crate::crablock_cmd::CrablockArgv;

const ENOSYS_COMPAT_LIST: &[&str] = &[
    "statx",
    "clone3",
    "close_range",
    "openat2",
    "faccessat2",
    "fchmodat2",
    // Not known by libseccomp 2.5.5
    //"setxattrat",
    //"getxattrat",
    //"listxattrat",
    //"removexattrat",
];
const ALWAYS_ALLOW_LIST: &[&str] = &[
    // Not known by libseccomp 2.5.5
    //"uretprobe"
];

#[derive(Debug, Default, Clone)]
pub(super) struct SyscallFilter {
    syscalls: HashSet<CompactString>,
    prctls: HashSet<CompactString>,
}
impl SyscallFilter {
    pub(super) fn allow(&mut self, syscall: CompactString) {
        if let Some(group_name) = syscall.strip_prefix('@') {
            self.syscalls.extend(
                syscall_groups::get(group_name)
                    .unwrap_or(&HashSet::new())
                    .iter()
                    .cloned(),
            );
        } else if let Some(prctl) = syscall.strip_prefix("prctl/") {
            if self.syscalls.insert("prctl".to_compact_string()) || !self.prctls.is_empty() {
                self.prctls
                    .insert("PR_".to_compact_string() + &prctl.to_compact_string().to_uppercase());
            }
        } else if syscall == "prctl" {
            self.syscalls.insert("prctl".to_compact_string());
            self.prctls = HashSet::new();
        } else {
            self.syscalls.insert(syscall.to_compact_string());
        }
    }

    pub(super) fn to_crablock_argv(&self, argv: &mut CrablockArgv<'_>) {
        let mut syscall_filter = String::with_capacity(self.syscalls.len() * 8);

        syscall_filter.push_str("*:EPERM");

        for &syscall in ENOSYS_COMPAT_LIST {
            if !self.syscalls.contains(syscall) {
                syscall_filter.push(',');
                syscall_filter.push_str(syscall);
                syscall_filter.push_str(":ENOSYS");
            }
        }

        for &syscall in ALWAYS_ALLOW_LIST {
            if !self.syscalls.contains(syscall) {
                syscall_filter.push(',');
                syscall_filter.push_str(syscall);
                syscall_filter.push_str(":allow");
            }
        }

        for syscall in &self.syscalls {
            syscall_filter.push(',');
            syscall_filter.push_str(syscall);
            syscall_filter.push_str(":allow");
        }

        argv.append(&clock_arg!["--seccomp-syscall-filter", syscall_filter]);
        argv.append(&clock_arg![
            "--seccomp-restrict-prctl",
            self.prctls
                .iter()
                .fold(String::new(), |acc, prctl| acc + prctl.as_str() + ",")
        ]);
    }
}

/*
 * Copyright © 2024,2025 The crabjail Authors
 *
 * This file is part of crabjail
 *
 * crabjail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabjail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::error::Error as StdError;

use crate::permissions::{
    ParseConnectivityError, ParseFsAccessError, ParseFsActionError, ParseSocketError,
};

#[derive(Debug, thiserror::Error)]
#[error(transparent)]
pub struct Error {
    #[from]
    inner: InnerError,
}
impl Error {
    #[expect(dead_code, reason = "Possible future use")]
    pub(crate) fn simple<M: Into<String>>(msg: M) -> Self {
        Self::from(InnerError::Simple {
            msg: msg.into(),
            source: None,
        })
    }

    #[expect(dead_code, reason = "Possible future use")]
    pub(crate) fn simple_with_source<M, E>(msg: M, source: E) -> Self
    where
        M: Into<String>,
        E: StdError + Send + Sync + 'static,
    {
        Self::from(InnerError::Simple {
            msg: msg.into(),
            source: Some(Box::new(source)),
        })
    }

    #[expect(dead_code, reason = "Possible future use")]
    pub(crate) fn from_io(io_err: std::io::Error) -> Self {
        Self {
            inner: InnerError::Io(io_err),
        }
    }
}
/*
impl<T: Into<InnerError>> From<T> for Error {
    fn from(e: T) -> Self {
        Self { inner: e.into() }
    }
}
*/

#[derive(Debug, thiserror::Error)]
pub(crate) enum InnerError {
    #[error("{msg}")]
    Simple {
        msg: String,
        #[source]
        source: Option<Box<dyn StdError + Send + Sync + 'static>>,
    },
    #[error("{0}")]
    Io(#[from] std::io::Error),
    #[error("{0}")]
    Toml(#[from] toml::de::Error),
}
impl InnerError {
    #[expect(dead_code, reason = "Possible future use")]
    pub(crate) fn simple<M: Into<String>>(msg: M) -> Self {
        Self::Simple {
            msg: msg.into(),
            source: None,
        }
    }

    #[expect(dead_code, reason = "Possible future use")]
    pub(crate) fn simple_with_source<M, E>(msg: M, source: E) -> Self
    where
        M: Into<String>,
        E: StdError + Send + Sync + 'static,
    {
        Self::Simple {
            msg: msg.into(),
            source: Some(Box::new(source)),
        }
    }

    pub(crate) fn simple_with_boxed_source<M>(
        msg: M,
        source: Box<dyn StdError + Send + Sync + 'static>,
    ) -> Self
    where
        M: Into<String>,
    {
        Self::Simple {
            msg: msg.into(),
            source: Some(source),
        }
    }
}
impl From<Error> for InnerError {
    fn from(error: Error) -> Self {
        error.inner
    }
}
impl From<ParseFsActionError> for InnerError {
    fn from(value: ParseFsActionError) -> Self {
        let _ = value;
        todo!()
    }
}
impl From<ParseFsAccessError> for InnerError {
    fn from(value: ParseFsAccessError) -> Self {
        let _ = value;
        todo!()
    }
}
impl From<ParseConnectivityError> for InnerError {
    fn from(value: ParseConnectivityError) -> Self {
        let _ = value;
        todo!()
    }
}
impl From<ParseSocketError> for InnerError {
    fn from(value: ParseSocketError) -> Self {
        let _ = value;
        todo!()
    }
}

/*
 * Copyright © 2024,2025 The crabjail Authors
 *
 * This file is part of crabjail
 *
 * crabjail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabjail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::borrow::Cow;
use std::env;
use std::iter;
use std::path::{Path, PathBuf};

use lnix::std_suppl::prelude::OsStrExt;

use crate::dirs;
use crate::dirs::UserDirs;

pub(crate) fn expand_path<'a, P: Into<Cow<'a, Path>>>(path: P) -> Cow<'a, Path> {
    let path = path.into();

    if !path.as_os_str().starts_with("@") {
        return path;
    }

    if let Ok(subpath) = path.strip_prefix("@{HOME}") {
        dirs::home_dir().join(subpath).into()
    } else if let Ok(subpath) = path.strip_prefix("@{TMPDIR}") {
        dirs::tmp_dir().join(subpath).into()
    } else if let Ok(subpath) = path.strip_prefix("@{PWD}") {
        env::current_dir().expect("@{PWD}").join(subpath).into()
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_CACHE_HOME}") {
        dirs::xdg_cache_home().join(subpath).into()
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_CONFIG_HOME}") {
        dirs::xdg_config_home().join(subpath).into()
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_DATA_HOME}") {
        dirs::xdg_data_home().join(subpath).into()
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_STAT_HOME}") {
        dirs::xdg_state_home().join(subpath).into()
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_RUNTIME_DIR}") {
        dirs::xdg_runtime_dir().join(subpath).into()
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_DESKTOP_DIR}") {
        if let Some(desktop_dir) = &UserDirs::get().desktop {
            desktop_dir.join(subpath).into()
        } else {
            path
        }
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_DOCUMENTS_DIR}") {
        if let Some(documents_dir) = &UserDirs::get().documents {
            documents_dir.join(subpath).into()
        } else {
            path
        }
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_DOWNLOAD_DIR}") {
        if let Some(download_dir) = &UserDirs::get().download {
            download_dir.join(subpath).into()
        } else {
            path
        }
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_MUSIC_DIR}") {
        if let Some(music_dir) = &UserDirs::get().music {
            music_dir.join(subpath).into()
        } else {
            path
        }
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_PICTURES_DIR}") {
        if let Some(pictures_dir) = &UserDirs::get().pictures {
            pictures_dir.join(subpath).into()
        } else {
            path
        }
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_PUBLICSHARE_DIR}") {
        if let Some(publicshare_dir) = &UserDirs::get().publicshare {
            publicshare_dir.join(subpath).into()
        } else {
            path
        }
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_TEMPLATES_DIR}") {
        if let Some(templates_dir) = &UserDirs::get().templates {
            templates_dir.join(subpath).into()
        } else {
            path
        }
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_VIDEOS_DIR}") {
        if let Some(videos_dir) = &UserDirs::get().videos {
            videos_dir.join(subpath).into()
        } else {
            path
        }
    /*
    } else if path.as_os_str() == "@{WAYLAND_DISPLAY}" {
        let wayland_display = PathBuf::from(
            env::var_os("WAYLAND_DISPLAY").unwrap_or_else(|| OsString::from("wayland-0")),
        );
        if wayland_display.is_absolute() {
            wayland_display.into()
        } else {
            xdg_runtime_dir().join(wayland_display).into()
        }
    */
    } else {
        path
    }
}

#[expect(dead_code, reason = "Possible future use")]
pub(crate) fn expand_path_many(path: &Path) -> Box<dyn Iterator<Item = PathBuf> + '_> {
    if !path.as_os_str().starts_with("@") {
        Box::new(iter::once(path.to_path_buf()))
    } else if let Ok(subpath) = path.strip_prefix("@{PATH}") {
        Box::new(
            dirs::executable_dirs()
                .iter()
                .map(move |executable_dir| executable_dir.join(subpath)),
        )
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_CONFIG_DIRS}") {
        Box::new(
            dirs::xdg_config_dirs()
                .iter()
                .map(move |config_dir| config_dir.join(subpath)),
        )
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_DATA_DIRS}") {
        Box::new(
            dirs::xdg_data_dirs()
                .iter()
                .map(move |data_dir| data_dir.join(subpath)),
        )
    } else {
        Box::new(iter::once(expand_path(path).into_owned()))
    }
}

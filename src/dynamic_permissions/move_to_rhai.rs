/*
 * Copyright © 2024,2025 The crabjail Authors
 *
 * This file is part of crabjail
 *
 * crabjail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabjail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::ffi::OsString;
use std::path::PathBuf;
use std::{env, fs};

use lnix::std_suppl::prelude::OsStrExt;

use crate::Jail;
use crate::dirs::xdg_runtime_dir;
use crate::dynamic_permissions::{Context, DynPermFnResult};
use crate::permissions::*;

pub(super) fn base_os(jail: &mut Jail, _ctx: &Context) -> DynPermFnResult {
    const BIN_DIRS: &[&str] = &[
        "/usr/bin",
        "/usr/sbin",
        "/usr/libexec",
        "/usr/local/bin",
        "/usr/local/sbin",
        "/usr/local/libexec",
        "/bin",
        "/sbin",
    ];
    // const operators are unstable
    const BIN_DIR_ACCESS: FsAccess = FsAccess::empty()
        .union(FsAccess::READ)
        .union(FsAccess::EXECUTE);

    const LIB_DIRS: &[&str] = &[
        "/usr/lib",
        "/usr/lib64",
        "/usr/local/lib",
        "/usr/local/lib64",
        "/lib",
        "/lib64",
    ];
    const LIB_DIR_ACCESS: FsAccess = FsAccess::empty()
        .union(FsAccess::READ)
        .union(FsAccess::LIST)
        .union(FsAccess::EXECUTE);

    const DATA_DIRS: &[&str] = &["/usr/share", "/usr/local/share"];
    const DATA_DIR_ACCESS: FsAccess = FsAccess::empty()
        .union(FsAccess::READ)
        .union(FsAccess::LIST);

    const SYSCONF_DIRS: &[&str] = &["/etc", "/usr/etc"];
    const SYSCONF_DIR_ACCESS: FsAccess = FsAccess::empty()
        .union(FsAccess::READ)
        .union(FsAccess::LIST);

    for (dirs, access) in [
        (BIN_DIRS, BIN_DIR_ACCESS),
        (LIB_DIRS, LIB_DIR_ACCESS),
        (DATA_DIRS, DATA_DIR_ACCESS),
        (SYSCONF_DIRS, SYSCONF_DIR_ACCESS),
    ] {
        for dir in dirs {
            jail.add(Permission::FsPath {
                path: PathBuf::from(dir),
                policy: FsPolicy {
                    action: FsAction::Allow,
                    access,
                },
            })?;
        }
    }

    jail.add(Permission::FsPath {
        path: PathBuf::from("/tmp"),
        policy: FsPolicy {
            action: FsAction::PrivateVolatile,
            access: FsAccess::MANAGE | FsAccess::LIST,
        },
    })?;

    jail.add(Permission::FsPath {
        path: PathBuf::from("/proc"),
        policy: FsPolicy {
            action: FsAction::Allow,
            access: FsAccess::READ,
        },
    })?;

    _common_devices(jail)?;

    Ok(())
}

fn _common_devices(jail: &mut Jail) -> DynPermFnResult {
    jail.add(Permission::FsPath {
        path: PathBuf::from("/dev"),
        policy: FsPolicy {
            action: FsAction::_MaskDev,
            access: FsAccess::empty(),
        },
    })?;

    // FIXME: /dev/console RO or RW?
    const RW_DEV: &[&str] = &["/dev/full", "/dev/null", "/dev/pts", "/dev/tty"];
    const RO_DEV: &[&str] = &["/dev/random", "/dev/urandom", "/dev/zero"];

    for path in RW_DEV {
        jail.add(Permission::FsPath {
            path: PathBuf::from(path),
            policy: FsPolicy {
                action: FsAction::Access,
                access: FsAccess::READ | FsAccess::WRITE | FsAccess::ACCESS_DEVICES,
            },
        })?;
    }
    for path in RO_DEV {
        jail.add(Permission::FsPath {
            path: PathBuf::from(path),
            policy: FsPolicy {
                action: FsAction::Access,
                access: FsAccess::READ | FsAccess::ACCESS_DEVICES,
            },
        })?;
    }

    /* FIXME
    if uses_terminals {
        jail.add(Permission::FsPath {
            path: PathBuf::from("/dev/tty"),
            policy: FsPolicy {
                action: FsAction::Access,
                access: FsAccess::LANDLOCK_IOCTL_DEV,
            },
        })?;
    }
    */

    let dev_shm_access = FsAccess::MANAGE | FsAccess::LIST;
    /* FIXME
    if dev_shm_exec {
        dev_shm_access |= FsAccess::EXECUTE;
    }
    */
    jail.add(Permission::FsPath {
        path: PathBuf::from("/dev/shm"),
        policy: FsPolicy {
            action: FsAction::Access,
            access: dev_shm_access,
        },
    })?;

    Ok(())
}

pub(super) fn pipewire(jail: &mut Jail, _ctx: &Context) -> DynPermFnResult {
    jail.add(Permission::FsPath {
        path: xdg_runtime_dir().join("pipewire-0"),
        policy: FsPolicy {
            action: FsAction::Allow,
            access: FsAccess::READ | FsAccess::WRITE,
        },
    })?;
    jail.add(Permission::NetSocket(Socket::Unix))?;

    Ok(())
}

pub(super) fn pulseaudio(jail: &mut Jail, _ctx: &Context) -> DynPermFnResult {
    jail.add(Permission::FsPath {
        path: xdg_runtime_dir().join("pulse"),
        policy: FsPolicy {
            action: FsAction::PrivateVolatile,
            access: FsAccess::READ | FsAccess::WRITE | FsAccess::LIST,
        },
    })?;
    jail.add(Permission::FsPath {
        path: xdg_runtime_dir().join("pulse/native"),
        policy: FsPolicy {
            action: FsAction::Allow,
            access: FsAccess::empty(),
        },
    })?;
    jail.add(Permission::NetSocket(Socket::Unix))?;

    Ok(())
}

pub(super) fn wayland(jail: &mut Jail, _ctx: &Context) -> DynPermFnResult {
    let wayland_display = PathBuf::from(
        env::var_os("WAYLAND_DISPLAY").unwrap_or_else(|| OsString::from("wayland-0")),
    );
    let wayland_socket = if wayland_display.is_absolute() {
        wayland_display
    } else {
        xdg_runtime_dir().join(wayland_display)
    };

    jail.add(Permission::FsPath {
        path: wayland_socket,
        policy: FsPolicy {
            action: FsAction::Allow,
            access: FsAccess::READ | FsAccess::WRITE,
        },
    })?;
    jail.add(Permission::NetSocket(Socket::Unix))?;
    jail.env("WAYLAND_DISPLAY", EnvAction::Keep);
    jail.env("XDG_SESSION_TYPE", EnvAction::Keep);

    Ok(())
}

pub(super) fn force_wayland(jail: &mut Jail, _ctx: &Context) -> DynPermFnResult {
    const VARIABLES: &[(&str, &str)] = &[
        ("CLUTTER_BACKEND", "wayland"),
        ("ELECTRON_OZONE_PLATFORM_HINT", "wayland"),
        ("GDK_BACKEND", "wayland"),
        ("MOZ_ENABLE_WAYLAND", "1"),
        ("QT_QPA_PLATFORM", "wayland"),
        ("SDL_VIDEODRIVER", "wayland"),
        ("XDG_SESSION_TYPE", "wayland"),
    ];
    for &(name, value) in VARIABLES {
        jail.env(name, EnvAction::Set(value.to_string()));
    }

    Ok(())
}

pub(super) fn use_portals(jail: &mut Jail, _ctx: &Context) -> DynPermFnResult {
    const VARIABLES: &[(&str, &str)] = &[
        ("GTK_USE_PORTAL", "1"),
        ("GIO_USE_PORTALS", "1"),
        ("GDK_DEBUG", "portals"),
        ("IBUS_USE_PORTAL", "1"),
        ("WEBKIT_USE_PORTAL", "1"),
        ("PLASMA_INTEGRATION_USE_PORTAL", "1"),
    ];
    for &(name, value) in VARIABLES {
        jail.env(name, EnvAction::Set(value.to_string()));
    }

    Ok(())
}

pub(super) fn cdrom(jail: &mut Jail, _ctx: &Context) -> DynPermFnResult {
    let _ = jail;
    unimplemented!()
}

/// - `/dev/dri/by-path`
/// - `/dev/dri/card1`
/// - `/dev/dri/renderD128`
/// - `/dev/char/226:1` -> `/dev/dri/card1` [legacy(?)]
/// - `/dev/char/226:128` -> `/dev/dri/renderD128` [legacy(?)]
///
/// - `/sys/dev/char/226:1` -> `/sys/devices/pci0000:00/0000:00:02.0/drm/card1`
/// - `/sys/dev/char/226:128` -> `/sys/devices/pci0000:00/0000:00:02.0/drm/renderD128`
/// - `/sys/class/drm`
///   - `/sys/class/drm/card1` -> `/sys/devices/pci0000:00/0000:00:02.0/drm/card1`
///   - `/sys/class/drm/card1-eDP-1` -> `/sys/devices/pci0000:00/0000:00:02.0/drm/card1/card1-eDP-1`
///   - `/sys/class/drm/card1-HDMI-A-1` -> `/sys/devices/pci0000:00/0000:00:02.0/drm/card1/card1-HDMI-A-1`
///   - `/sys/class/drm/renderD128` -> `/sys/devices/pci0000:00/0000:00:02.0/drm/renderD128`
/// - `/sys/bus/pci/devices/0000:00:02.0` -> `/sys/devices/pci0000:00/0000:00:02.0`
/// - `/sys/bus/pci/drivers/i915`
///   - `/sys/bus/pci/drivers/i915/0000:00:02.0` -> `/sys/devices/pci0000:00/0000:00:02.0`
/// - `/sys/devices/pci0000:00/0000:00:02.0`
///   - `/sys/devices/pci0000:00/0000:00:02.0/drm/card1/dev` -: `26:1`
///   - `/sys/devices/pci0000:00/0000:00:02.0/drm/card1/device` -> `/sys/devices/pci0000:00/0000:00:02.0`
///   - `/sys/devices/pci0000:00/0000:00:02.0/drm/card1/subsystem` -> `/sys/class/drm`
///   - `/sys/devices/pci0000:00/0000:00:02.0/drm/card1/uevent` -: `MAJOR=226 MINOR=1 DEVNAME=dri/card1`
///   - `/sys/devices/pci0000:00/0000:00:02.0/drm/renderD128/dev` -: `26:128`
///   - `/sys/devices/pci0000:00/0000:00:02.0/drm/renderD128/device` -> `/sys/devices/pci0000:00/0000:00:02.0`
///   - `/sys/devices/pci0000:00/0000:00:02.0/drm/renderD128/subsystem` -> `/sys/class/drm`
///   - `/sys/devices/pci0000:00/0000:00:02.0/drm/renderD128/uevent` -: `MAJOR=226 MINOR=128 DEVNAME=dri/renderD128`
///   - `/sys/devices/pci0000:00/0000:00:02.0/subsystem` -> `/sys/bus/pci`
pub(super) fn drm(jail: &mut Jail, _ctx: &Context) -> DynPermFnResult {
    jail.add(Permission::FsPath {
        path: PathBuf::from("/dev/dri"),
        policy: FsPolicy {
            action: FsAction::Allow,
            access: FsAccess::READ
                | FsAccess::WRITE
                | FsAccess::LIST
                | FsAccess::ACCESS_DEVICES
                | FsAccess::LANDLOCK_IOCTL_DEV,
        },
    })?;

    jail.add(Permission::FsPath {
        path: PathBuf::from("/sys/class/drm"),
        policy: FsPolicy {
            action: FsAction::Allow,
            access: FsAccess::READ | FsAccess::LIST,
        },
    })?;
    // self.add(
    //     "/sys/dev/char",
    //     FsAction::Access,
    //     FsAccess::LIST,
    // );
    for dentry in fs::read_dir("/sys/class/drm")? {
        let dentry = dentry?;
        if !dentry.file_type()?.is_symlink() || dentry.file_name().contains(b'-') {
            continue;
        }
        let path = dentry.path();
        let major_minor = {
            let mut mm = fs::read_to_string(path.join("dev"))?;
            mm.pop();
            mm
        };

        jail.add(Permission::FsPath {
            path: path.clone(),
            policy: FsPolicy {
                action: FsAction::AllowSymlinkTarget,
                access: FsAccess::READ,
            },
        })?;
        jail.add(Permission::FsPath {
            path: path.join("device"),
            policy: FsPolicy {
                action: FsAction::AllowSymlinkTarget,
                access: FsAccess::READ,
            },
        })?;
        jail.add(Permission::FsPath {
            path: PathBuf::from(format!("/sys/dev/char/{major_minor}")),
            policy: FsPolicy {
                action: FsAction::Allow,
                access: FsAccess::empty(),
            },
        })?;
    }
    Ok(())
}

pub(super) fn misc_udmabuf(jail: &mut Jail, _ctx: &Context) -> DynPermFnResult {
    jail.add(Permission::FsPath {
        path: PathBuf::from("/dev/udmabuf"),
        policy: FsPolicy {
            action: FsAction::Allow,
            access: FsAccess::READ
                | FsAccess::WRITE
                | FsAccess::ACCESS_DEVICES
                | FsAccess::LANDLOCK_IOCTL_DEV,
        },
    })?;
    let major_minor = {
        let mut mm = fs::read_to_string("/sys/class/misc/udmabuf/dev")?;
        mm.pop();
        mm
    };
    jail.add(Permission::FsPath {
        path: PathBuf::from("/sys/class/misc/udmabuf"),
        policy: FsPolicy {
            action: FsAction::AllowAndSymlinkTarget,
            access: FsAccess::empty(),
        },
    })?;
    jail.add(Permission::FsPath {
        path: PathBuf::from(format!("/sys/dev/char/{major_minor}")),
        policy: FsPolicy {
            action: FsAction::Allow,
            access: FsAccess::empty(),
        },
    })?;
    Ok(())
}

/// - `/dev/v4l`
/// - `/dev/video0`
/// - `/dev/video1`
/// - `/dev/char/81:0` -> `/dev/video0` [legacy(?)]
/// - `/dev/char/81:1` -> `/dev/video1` [legacy(?)]
///
/// - `/sys/dev/char/81:0` -> `/sys/devices/pci0000:00/0000:00:14.0/usb1/1-6/1-6:1.0/video4linux/video0`
/// - `/sys/dev/char/81:1` -> `/sys/devices/pci0000:00/0000:00:14.0/usb1/1-6/1-6:1.0/video4linux/video1`
/// - `/sys/class/video4linux`
///   - `/sys/class/video4linux/video0` -> `/sys/devices/pci0000:00/0000:00:14.0/usb1/1-6/1-6:1.0/video4linux/video0`
///   - `/sys/class/video4linux/video1` -> `/sys/devices/pci0000:00/0000:00:14.0/usb1/1-6/1-6:1.0/video4linux/video1`
/// - `/sys/bus/pci/devices/0000:00:14.0` -> `/sys/devices/pci0000:00/0000:00:14.0`
/// - `/sys/bus/pci/drivers/xhci_hcd/0000:00:14.0` -> `/sys/devices/pci0000:00/0000:00:14.0`
/// - `/sys/bus/usb/devices/1-6` -> `/sys/devices/pci0000:00/0000:00:14.0/usb1/1-6`
/// - `/sys/bus/usb/devices/1-6:1.0` -> `/sys/devices/pci0000:00/0000:00:14.0/usb1/1-6/1-6:1.0`
/// - `/sys/bus/usb/devices/usb1` -> `/sys/devices/pci0000:00/0000:00:14.0/usb1`
/// - `/sys/bus/usb/drivers/uvcvideo/1-6:1.0` -> `/sys/devices/pci0000:00/0000:00:14.0/usb1/1-6/1-6:1.0`
/// - `/sys/bus/usb/drivers/uvcvideo/1-6:1.1` -> `/sys/devices/pci0000:00/0000:00:14.0/usb1/1-6/1-6:1.1`
/// - `/sys/bus/media/devices/media0` -> `/sys/devices/pci0000:00/0000:00:14.0/usb1/1-6/1-6:1.0/media0`
/// - `/sys/devices/pci0000:00/0000:00:14.0`
///   - `/sys/devices/pci0000:00/0000:00:14.0/usb1/dev` -: `189:0`
///   - `/sys/devices/pci0000:00/0000:00:14.0/usb1/subsystem` -> `/sys/bus/usb`
///   - `/sys/devices/pci0000:00/0000:00:14.0/usb1/uevent` -: `DEVNAME=bus/usb/001/001`
///     - `/sys/devices/pci0000:00/0000:00:14.0/usb1/1-6/dev` -: `189:2`
///     - `/sys/devices/pci0000:00/0000:00:14.0/usb1/1-6/port` -> `/sys/devices/pci0000:00/0000:00:14.0/usb1/1-0:1.0/usb1-port6`
///     - `/sys/devices/pci0000:00/0000:00:14.0/usb1/1-6/subsystem` -> `/sys/bus/usb`
///     - `/sys/devices/pci0000:00/0000:00:14.0/usb1/1-6/uevent` -: `DEVNAME=bus/usb/001/003`
///       - `/sys/devices/pci0000:00/0000:00:14.0/usb1/1-6/1-6:1.0/media0/dev` -: `235:0`
///       - `/sys/devices/pci0000:00/0000:00:14.0/usb1/1-6/1-6:1.0/media0/uevent` -: `DEVNAME=media0`
///       - `/sys/devices/pci0000:00/0000:00:14.0/usb1/1-6/1-6:1.0/video4linux/video0/dev` -: `81:0`
///       - `/sys/devices/pci0000:00/0000:00:14.0/usb1/1-6/1-6:1.0/video4linux/video0/uevent` -: `DEVNAME=video0`
///       - `/sys/devices/pci0000:00/0000:00:14.0/usb1/1-6/1-6:1.0/video4linux/video1/dev` -: `81:1`
///       - `/sys/devices/pci0000:00/0000:00:14.0/usb1/1-6/1-6:1.0/video4linux/video1/uevent` -: `DEVNAME=video1`
///   - `/sys/devices/pci0000:00/0000:00:14.0/subsystem` -> `/sys/bus/pci`
pub(super) fn video4linux(jail: &mut Jail, _ctx: &Context) -> DynPermFnResult {
    jail.add(Permission::FsPath {
        path: PathBuf::from("/dev/v4l"),
        policy: FsPolicy {
            action: FsAction::Allow,
            access: FsAccess::LIST,
        },
    })?;
    jail.add(Permission::FsPath {
        path: PathBuf::from("/sys/class/video4linux"),
        policy: FsPolicy {
            action: FsAction::Allow,
            access: FsAccess::READ | FsAccess::LIST,
        },
    })?;
    // self.add(
    //     "/sys/dev/char",
    //     FsAction::Access,
    //     FsAccess::LIST,
    // );
    for dentry in fs::read_dir("/sys/class/video4linux")? {
        let dentry = dentry?;
        if !dentry.file_type()?.is_symlink() {
            continue;
        }
        let path = dentry.path();
        let major_minor = {
            let mut mm = fs::read_to_string(path.join("dev"))?;
            mm.pop();
            mm
        };
        let devname = fs::read_to_string(path.join("uevent"))?
            .lines()
            .find_map(|l| l.strip_prefix("DEVNAME="))
            .unwrap()
            .to_string();

        jail.add(Permission::FsPath {
            path: path.clone(),
            policy: FsPolicy {
                action: FsAction::AllowAndSymlinkTarget,
                access: FsAccess::READ,
            },
        })?;
        jail.add(Permission::FsPath {
            path: PathBuf::from(format!("/sys/dev/char/{major_minor}")),
            policy: FsPolicy {
                action: FsAction::Allow,
                access: FsAccess::empty(),
            },
        })?;
        jail.add(Permission::FsPath {
            path: PathBuf::from(format!("/dev/{devname}")),
            policy: FsPolicy {
                action: FsAction::Allow,
                access: FsAccess::READ
                    | FsAccess::WRITE
                    | FsAccess::ACCESS_DEVICES
                    | FsAccess::LANDLOCK_MAYBE_IOCTL_DEV,
            },
        })?;
    }

    Ok(())
}

pub(super) fn http(jail: &mut Jail, _ctx: &Context) -> DynPermFnResult {
    jail.add(Permission::NetConnectTcp(TcpPort::Port(80)))?;
    jail.add(Permission::NetConnectivity(Connectivity::Pasta))?;
    jail.add(Permission::NetSocket(Socket::Ip))?;

    Ok(())
}

pub(super) fn https(jail: &mut Jail, _ctx: &Context) -> DynPermFnResult {
    jail.add(Permission::NetConnectTcp(TcpPort::Port(443)))?;
    jail.add(Permission::NetConnectivity(Connectivity::Pasta))?;
    jail.add(Permission::NetSocket(Socket::Ip))?;

    Ok(())
}

pub(super) fn webrtc(jail: &mut Jail, _ctx: &Context) -> DynPermFnResult {
    // 3478 is STUN/TURN
    jail.add(Permission::NetConnectTcp(TcpPort::Port(3478)))?;
    jail.add(Permission::NetConnectivity(Connectivity::Pasta))?;
    jail.add(Permission::NetSocket(Socket::Ip))?;
    jail.add(Permission::NetSocket(Socket::NetlinkRoute))?;

    Ok(())
}

fn _portal_common(jail: &mut Jail) -> DynPermFnResult {
    jail.add(Permission::SessionBusTalkRestricted {
        name: "org.freedesktop.portal.Desktop".to_string(),
        rule: DBusFilterRule {
            method: Some("org.freedesktop.Session.*".to_string()),
            path: Some("/org/freedesktop/portal/desktop".to_string()),
            call: true,
            broadcast: true,
        },
    })?;
    jail.add(Permission::SessionBusTalkRestricted {
        name: "org.freedesktop.portal.Desktop".to_string(),
        rule: DBusFilterRule {
            method: Some("org.freedesktop.Request.*".to_string()),
            path: Some("/org/freedesktop/portal/desktop".to_string()),
            call: true,
            broadcast: true,
        },
    })?;

    Ok(())
}

pub(super) fn file_transfer(jail: &mut Jail, _ctx: &Context) -> DynPermFnResult {
    _portal_common(jail)?;
    jail.add(Permission::SessionBusTalkRestricted {
        name: "org.freedesktop.portal.Desktop".to_string(),
        rule: DBusFilterRule {
            method: Some("org.freedesktop.FileTransfer.*".to_string()),
            path: Some("/org/freedesktop/portal/desktop".to_string()),
            call: true,
            broadcast: true,
        },
    })?;

    Ok(())
}
pub(super) fn screen_cast(jail: &mut Jail, _ctx: &Context) -> DynPermFnResult {
    _portal_common(jail)?;
    jail.add(Permission::SessionBusTalkRestricted {
        name: "org.freedesktop.portal.Documents".to_string(),
        rule: DBusFilterRule {
            method: Some("org.freedesktop.ScreenCast.*".to_string()),
            path: Some("/org/freedesktop/portal/desktop".to_string()),
            call: true,
            broadcast: true,
        },
    })?;

    Ok(())
}
pub(super) fn settings(jail: &mut Jail, _ctx: &Context) -> DynPermFnResult {
    _portal_common(jail)?;
    jail.add(Permission::SessionBusTalkRestricted {
        name: "org.freedesktop.portal.Desktop".to_string(),
        rule: DBusFilterRule {
            method: Some("org.freedesktop.Settings.*".to_string()),
            path: Some("/org/freedesktop/portal/desktop".to_string()),
            call: true,
            broadcast: true,
        },
    })?;

    Ok(())
}

/*
 * Copyright © 2024,2025 The crabjail Authors
 *
 * This file is part of crabjail
 *
 * crabjail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabjail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::borrow::Cow;
use std::env;
use std::ffi::{OsStr, OsString};
use std::os::unix::prelude::*;
use std::path::Path;
use std::process::ExitCode;
use std::time::SystemTime;

use clap::Parser;
use crabjail::JailFile;
use crabjail::dirs::home_dir;
use crabjail::permissions::{FsAccess, FsAction, FsPolicy, Permission};
use simplelog::{ColorChoice, Config, TermLogger, TerminalMode};

use crate::cli::{Args, Debug};

mod cli;

fn main() -> ExitCode {
    let args = Args::parse();

    TermLogger::init(
        args.log_level,
        Config::default(),
        TerminalMode::Mixed,
        ColorChoice::Auto,
    )
    .expect("Failed to initialize logger");

    crabjail::set_app_id("page.codeberg.crabjail.Crabjail");

    // ---

    let jailfile_path = args.jail.as_deref().unwrap_or(&args.argv[0]);
    let jailfile = match JailFile::open(jailfile_path) {
        Ok(jailfile) => jailfile,
        Err(err) => {
            log::error!("An error occurred while reading {jailfile_path}: {err}");
            return ExitCode::FAILURE;
        }
    };
    let mut jail = match jailfile.to_jail() {
        Ok(jail) => jail,
        Err(err) => {
            log::error!("An error occurred while creating the jail: {err}");
            return ExitCode::FAILURE;
        }
    };

    if let Some(cjail_section) = jailfile.get_third_party_section("cjail") {
        if let Some(filesystem_from_cmd) = cjail_section.get("filesystem-from-cmd") {
            match filesystem_from_cmd {
                toml::Value::String(s) if s == "read" => {
                    for arg in &args.argv[1..] {
                        let arg = if arg.starts_with("--") {
                            if let Some((_, arg)) = arg.split_once('=') {
                                arg
                            } else {
                                continue;
                            }
                        } else {
                            arg
                        };
                        let arg = if let Some(arg) = arg.strip_prefix("~/") {
                            assert!(!arg.starts_with('/'));
                            Cow::Owned(home_dir().join(arg))
                        } else {
                            Cow::Borrowed(Path::new(arg))
                        };
                        if arg.exists() {
                            jail.add(Permission::FsPath {
                                path: arg.into_owned(),
                                policy: FsPolicy {
                                    action: FsAction::Allow,
                                    access: FsAccess::READ,
                                },
                            })
                            .expect("jail.add");
                        }
                    }
                }
                _ => unimplemented!(),
            }
        }
    }

    let mut command = jail.crablock_cmd().log_argv().to_command();

    if args.debug.is_some_and(|d| d == Debug::Shell) {
        let shell: OsString;
        let debug_shell: &OsStr = match crabjail::config::get_config_section("cjail")
            .and_then(|m| m.get("debug_shell"))
            .and_then(|v| v.as_str())
        {
            Some("$SHELL") => {
                shell = env::var_os("SHELL").unwrap_or_else(|| OsString::from("/bin/sh"));
                shell.as_os_str()
            }
            Some(debug_shell) => OsStr::new(debug_shell),
            None => OsStr::new("/bin/sh"),
        };
        command.arg(debug_shell);
    } else if args.debug.is_some_and(|d| d == Debug::Strace) {
        command.arg("strace");
        command.arg("-o");
        command.arg(format!(
            // FIXME: Move to function and log::info
            "/tmp/cjail-debug-strace/{}-{}.strace",
            &args.argv[0],
            SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_secs()
        ));
        command.args(
            crabjail::config::get_config_section("cjail")
                .expect("cjail section should always be present")
                .get("debug_strace_argv")
                .expect("debug_strace_argv should always be set")
                .as_array()
                .expect("debug_strace_argv must be an array")
                .iter()
                .map(|a| {
                    a.as_str()
                        .expect("debug_strace_argv must be an array of strings")
                }),
        );
        command.arg("--");
        command.args(args.argv);
    } else {
        command.args(args.argv);
    }
    let _ = command.exec();

    ExitCode::FAILURE
}

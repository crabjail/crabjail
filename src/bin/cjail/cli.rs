/*
 * Copyright © 2024,2025 The crabjail Authors
 *
 * This file is part of crabjail
 *
 * crabjail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabjail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use clap::{Parser, ValueEnum};
use log::LevelFilter;

#[derive(Debug, Parser)]
#[command(version)]
pub(crate) struct Args {
    /// The jail to use. If not specified, it is inferred from the command.
    #[arg(long)]
    pub jail: Option<String>,

    #[arg(long, default_value = "info")]
    pub log_level: LevelFilter,

    #[arg(long)]
    pub debug: Option<Debug>,

    #[arg(required = true, trailing_var_arg = true, value_name = "COMMAND")]
    pub argv: Vec<String>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, ValueEnum)]
pub(crate) enum Debug {
    Shell,
    Strace,
}

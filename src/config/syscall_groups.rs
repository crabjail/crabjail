/*
 * Copyright © 2024,2025 The crabjail Authors
 *
 * This file is part of crabjail
 *
 * crabjail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabjail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::collections::{HashMap, HashSet};
use std::mem;
use std::sync::OnceLock;

use compact_str::{CompactString, ToCompactString};
use either::Either;

use crate::config::get_merged_config_files;

static SYSCALL_GROUPS: OnceLock<HashMap<CompactString, HashSet<CompactString>>> = OnceLock::new();

pub(crate) fn get(name: &str) -> Option<&'static HashSet<CompactString>> {
    if let Some(syscall_groups) = SYSCALL_GROUPS.get() {
        return syscall_groups.get(name);
    }

    let all_groups: HashMap<CompactString, HashSet<CompactString>> =
        get_merged_config_files("syscall-groups.toml");

    let mut syscall_groups: HashMap<CompactString, HashSet<CompactString>> = HashMap::new();
    let mut unresolved_syscall_groups: HashMap<CompactString, (Vec<_>, HashSet<CompactString>)> =
        HashMap::new();
    for (name, syscalls) in all_groups {
        let (groups, syscalls) = syscalls
            .into_iter()
            .map(|s| {
                if let Some(group) = s.strip_prefix('@') {
                    Either::Left(group.to_compact_string())
                } else {
                    Either::Right(s.to_compact_string())
                }
            })
            .fold((Vec::new(), HashSet::new()), |mut acc, e| {
                match e {
                    Either::Left(group) => acc.0.push(group),
                    Either::Right(syscall) => {
                        acc.1.insert(syscall);
                    }
                }
                acc
            });
        if groups.is_empty() {
            syscall_groups.insert(name, syscalls);
        } else {
            unresolved_syscall_groups.insert(name, (groups, syscalls));
        }
    }
    let mut depth = 0;
    while !unresolved_syscall_groups.is_empty() {
        depth += 1;
        if depth > 6 {
            log::error!(
                "Reached maximum depth while resolving syscall groups. This is likely caused by including an unknown group (check for typos) or a recursive/cylic include.",
            );
            for (name, (groups, _)) in &unresolved_syscall_groups {
                log::error!(" * unresolved groups in group \"{name}\": {:?}", groups);
            }
            panic!("Reached maximum depth while resolving syscall groups.");
        }

        unresolved_syscall_groups.retain(|name, (groups, syscalls)| {
            groups.retain(|group| {
                if let Some(more_syscalls) = syscall_groups.get(group) {
                    syscalls.extend(more_syscalls.iter().cloned());
                    false
                } else {
                    true
                }
            });
            if groups.is_empty() {
                syscall_groups.insert(name.clone(), mem::take(syscalls));
                false
            } else {
                true
            }
        });
    }

    let _ = SYSCALL_GROUPS.set(syscall_groups);

    get(name)
}

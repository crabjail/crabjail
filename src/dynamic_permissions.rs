/*
 * Copyright © 2024,2025 The crabjail Authors
 *
 * This file is part of crabjail
 *
 * crabjail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabjail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use core::fmt;
use std::borrow::Borrow;
use std::collections::HashMap;
use std::error::Error as StdError;
use std::hash::{BuildHasherDefault, DefaultHasher};
use std::sync::RwLock;

use compact_str::CompactString;

use crate::Jail;

mod move_to_rhai;

pub static DYNAMIC_PERMISSIONS: RwLock<DynamicPermissions> =
    RwLock::new(DynamicPermissions::default());

pub type DynPermFnResult = Result<(), Box<dyn StdError + Send + Sync>>;
pub type DynPermFn = Box<dyn Fn(&mut Jail, &Context) -> DynPermFnResult + Send + Sync>;

#[derive(Debug, Clone, Hash)]
#[non_exhaustive]
pub struct Context {
    _private: (),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[non_exhaustive]
pub enum DynPermissionType {
    Permission,
    DeviceGroup,
    Network,
    Portal,
}

pub struct DynamicPermissions {
    initialized: bool,
    permission: HashMap<CompactString, DynPermFn, BuildHasherDefault<DefaultHasher>>,
    device_group: HashMap<CompactString, DynPermFn, BuildHasherDefault<DefaultHasher>>,
    network: HashMap<CompactString, DynPermFn, BuildHasherDefault<DefaultHasher>>,
    portal: HashMap<CompactString, DynPermFn, BuildHasherDefault<DefaultHasher>>,
}
impl DynamicPermissions {
    const fn default() -> Self {
        Self {
            initialized: false,
            permission: HashMap::with_hasher(BuildHasherDefault::new()),
            device_group: HashMap::with_hasher(BuildHasherDefault::new()),
            network: HashMap::with_hasher(BuildHasherDefault::new()),
            portal: HashMap::with_hasher(BuildHasherDefault::new()),
        }
    }

    pub fn init(&mut self) {
        if !self.initialized {
            self._init();
        }
    }

    pub fn reinit(&mut self) {
        unimplemented!()
    }

    fn _init(&mut self) {
        self.register(
            DynPermissionType::Permission,
            "base-os",
            Box::new(move_to_rhai::base_os),
        );
        self.register(
            DynPermissionType::Permission,
            "pipewire",
            Box::new(move_to_rhai::pipewire),
        );
        self.register(
            DynPermissionType::Permission,
            "pulseaudio",
            Box::new(move_to_rhai::pulseaudio),
        );
        self.register(
            DynPermissionType::Permission,
            "wayland",
            Box::new(move_to_rhai::wayland),
        );
        self.register(
            DynPermissionType::Permission,
            "force-wayland",
            Box::new(move_to_rhai::force_wayland),
        );
        self.register(
            DynPermissionType::Permission,
            "use-portals",
            Box::new(move_to_rhai::use_portals),
        );

        self.register(
            DynPermissionType::DeviceGroup,
            "cdrom",
            Box::new(move_to_rhai::cdrom),
        );
        self.register(
            DynPermissionType::DeviceGroup,
            "drm",
            Box::new(move_to_rhai::drm),
        );
        self.register(
            DynPermissionType::DeviceGroup,
            "misc/udmabuf",
            Box::new(move_to_rhai::misc_udmabuf),
        );
        self.register(
            DynPermissionType::DeviceGroup,
            "videl4linux",
            Box::new(move_to_rhai::video4linux),
        );

        self.register(
            DynPermissionType::Network,
            "http",
            Box::new(move_to_rhai::http),
        );
        self.register(
            DynPermissionType::Network,
            "https",
            Box::new(move_to_rhai::https),
        );
        self.register(
            DynPermissionType::Network,
            "webrtc",
            Box::new(move_to_rhai::webrtc),
        );

        self.register(
            DynPermissionType::Portal,
            "FileTransfer",
            Box::new(move_to_rhai::file_transfer),
        );
        // TODO: dependency: "pipewire"
        self.register(
            DynPermissionType::Portal,
            "ScreenCast",
            Box::new(move_to_rhai::screen_cast),
        );
        self.register(
            DynPermissionType::Portal,
            "Settings",
            Box::new(move_to_rhai::settings),
        );
    }

    pub fn register<S>(&mut self, type_: DynPermissionType, name: S, func: DynPermFn)
    where
        S: Into<CompactString>,
    {
        match type_ {
            DynPermissionType::Permission => {
                if self.permission.insert(name.into(), func).is_some() {
                    unimplemented!();
                }
            }
            DynPermissionType::DeviceGroup => {
                if self.device_group.insert(name.into(), func).is_some() {
                    unimplemented!();
                }
            }
            DynPermissionType::Network => {
                if self.network.insert(name.into(), func).is_some() {
                    unimplemented!();
                }
            }
            DynPermissionType::Portal => {
                if self.network.insert(name.into(), func).is_some() {
                    unimplemented!();
                }
            }
        }
    }

    pub fn apply_permission<S>(&self, name: S, jail: &mut Jail) -> DynPermFnResult
    where
        S: Borrow<str>,
    {
        let name = name.borrow();
        let Some(func) = self.permission.get(name) else {
            unimplemented!()
        };
        func(jail, &Context { _private: () })
    }

    pub fn apply_device_group<S>(&self, name: S, jail: &mut Jail) -> DynPermFnResult
    where
        S: Borrow<str>,
    {
        let name = name.borrow();
        let Some(func) = self.device_group.get(name) else {
            unimplemented!()
        };
        func(jail, &Context { _private: () })
    }

    pub fn apply_network<S>(&self, name: S, jail: &mut Jail) -> DynPermFnResult
    where
        S: Borrow<str>,
    {
        let name = name.borrow();
        let Some(func) = self.network.get(name) else {
            unimplemented!()
        };
        func(jail, &Context { _private: () })
    }

    pub fn apply_portal<S>(&self, name: S, jail: &mut Jail) -> DynPermFnResult
    where
        S: Borrow<str>,
    {
        let name = name.borrow();
        let Some(func) = self.portal.get(name) else {
            unimplemented!()
        };
        func(jail, &Context { _private: () })
    }
}
impl fmt::Debug for DynamicPermissions {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("DynamicPermissions")
            .field("initialized", &self.initialized)
            // field_with and from_fn are unstable, hence we can not simply use DebugMap.
            // Therefore we simplify to just list the keys.
            .field(
                "permissions",
                &self
                    .permission
                    .keys()
                    .fold(String::new(), |acc, key| acc + &**key + ","),
            )
            .field(
                "device_group",
                &self
                    .device_group
                    .keys()
                    .fold(String::new(), |acc, key| acc + &**key + ","),
            )
            .field(
                "network",
                &self
                    .network
                    .keys()
                    .fold(String::new(), |acc, key| acc + &**key + ","),
            )
            .field(
                "portal",
                &self
                    .portal
                    .keys()
                    .fold(String::new(), |acc, key| acc + &**key + ","),
            )
            .finish()?;

        Ok(())
    }
}

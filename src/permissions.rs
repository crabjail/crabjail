/*
 * Copyright © 2024,2025 The crabjail Authors
 *
 * This file is part of crabjail
 *
 * crabjail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabjail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::num::ParseIntError;
use std::path::PathBuf;
use std::str::FromStr;

use bitflags::bitflags;
use compact_str::CompactString;

#[derive(Debug)]
#[non_exhaustive]
pub enum Permission {
    FsPath { path: PathBuf, policy: FsPolicy },
    FsFullProc,
    NetBindTcp(TcpPort),
    NetConnectTcp(TcpPort),
    NetConnectivity(Connectivity),
    NetSocket(Socket),
    Syscall(CompactString),
    NamespacesLimit(Namespace, Option<u8>),
    MemoryAllowWriteExecute,
    SessionBusOwn { name: String },
    SessionBusTalk { name: String },
    SessionBusTalkRestricted { name: String, rule: DBusFilterRule },
    SessionBusSee { name: String },
    SystemBusOwn { name: String },
    SystemBusTalk { name: String },
    SystemBusTalkRestricted { name: String, rule: DBusFilterRule },
    SystemBusSee { name: String },
}
/*
impl FromStr for Permission {
    type Err = ParsePermissionError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        (|| -> Result<_, ParsePermissionErrorRepr> {
            if s == "fs.full-proc" {
                Ok(Self::FsFullProc)
            } else if let Some(s) = s.strip_prefix("fs.") {
                let (action, s) = if let Some(s) = s.strip_prefix("allow ") {
                    (FsAction::Allow, s)
                } else if let Some(s) = s.strip_prefix("private ") {
                    (FsAction::Private, s)
                } else if let Some(s) = s.strip_prefix("private-volatile ") {
                    (FsAction::PrivateVolatile, s)
                } else {
                    return Err(ParsePermissionErrorRepr::Simple(format!(
                        "bad fs.<action> `{}': unknown",
                        s.split_once(' ').unwrap_or((s, "")).0
                    )));
                };
                let Some((access, path)) = s.split_once(' ') else {
                    return Err(ParsePermissionErrorRepr::Simple(format!(
                        "expected fs.ACTION [ACCESS] PATH, found `{s}'",
                    )));
                };
                Ok(Self::FsPath {
                    path: PathBuf::from(path),
                    policy: FsPolicy {
                        action,
                        access: FsAccess::from_str(access)?,
                    },
                })
            } else if let Some(port) = s.strip_prefix("net.bind-tcp ") {
                Ok(Self::NetBindTcp(TcpPort::from_str(port)?))
            } else if let Some(port) = s.strip_prefix("net.connect-tcp ") {
                Ok(Self::NetConnectTcp(TcpPort::from_str(port)?))
            } else if let Some(mode) = s.strip_prefix("net.connectivity ") {
                Ok(Self::NetConnectivity(Connectivity::from_str(mode)?))
            } else if let Some(socket) = s.strip_prefix("net.socket ") {
                Ok(Self::NetSocket(Socket::from_str(socket)?))
            /* } else if let Some(syscall) = s.strip_prefix("syscall ") {
            Ok(Self::Syscall(Syscall::from_str(syscall)?)) */
            } else if let Some(namespaces_limit) = s.strip_prefix("namespaces-limit ") {
                let Some((namespace, count)) = namespaces_limit.split_once(':') else {
                    return Err(ParsePermissionErrorRepr::Simple(format!(
                        "expected namespaces-limit NAMESPACE:LIMIT, found `{s}'"
                    )));
                };
                Ok(Self::NamespacesLimit(
                    Namespace::from_str(namespace)?,
                    if count == "inf" {
                        None
                    } else {
                        Some(s.parse::<u8>()?)
                    },
                ))
            } else if s == "memory-allow-write-execute" {
                Ok(Self::MemoryAllowWriteExecute)
            } else {
                unimplemented!()
            }
        })()
        .map_err(From::from)
    }
}
#[derive(Debug, thiserror::Error)]
#[error(transparent)]
pub struct ParsePermissionError(#[from] ParsePermissionErrorRepr);
#[derive(Debug, thiserror::Error)]
enum ParsePermissionErrorRepr {
    #[error("bad permission: {0}")]
    Simple(String),
    #[error("bad permission: {0}")]
    ParseIntError(#[from] ParseIntError),
    #[error("bad permission: {0}")]
    ParseFsAccessError(#[from] ParseFsAccessError),
    #[error("bad permission: {0}")]
    ParseTcpPortError(#[from] ParseTcpPortError),
    #[error("bad permission: {0}")]
    ParseConnectivityError(#[from] ParseConnectivityError),
    #[error("bad permission: {0}")]
    ParseSocketError(#[from] ParseSocketError),
    #[error("bad permission: {0}")]
    ParseNamespaceError(#[from] ParseNamespaceError),
}
*/

#[derive(Debug, Clone)]
pub struct FsPolicy {
    pub action: FsAction,
    pub access: FsAccess,
}

#[derive(Debug, Clone, PartialEq, Eq)]
#[non_exhaustive]
pub enum FsAction {
    Allow,
    /// Unstable!
    AllowSymlinkTarget,
    /// Unstable!
    AllowAndSymlinkTarget,
    Private,
    PrivateVolatile,
    /// Unstable!
    Access,
    #[doc(hidden)]
    _MaskDev,
}
impl FromStr for FsAction {
    type Err = ParseFsActionError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "allow" => Ok(Self::Allow),
            "allow-symlink-target" => Ok(Self::AllowSymlinkTarget),
            "allow-and-symlink-target" => Ok(Self::AllowAndSymlinkTarget),
            "private" => Ok(Self::Private),
            "private-volatile" => Ok(Self::PrivateVolatile),
            "access" => Ok(Self::Access),
            _ => Err(ParseFsActionError(s.to_string())),
        }
    }
}
#[derive(Debug, thiserror::Error)]
#[error("bad fs-action `{0}': unknown")]
pub struct ParseFsActionError(String);

bitflags! {
    #[derive(Debug, Clone, Copy,PartialEq, Eq)]
    pub struct FsAccess: u32 {
        const READ = 1 << 0;
        const WRITE = 1 << 1;
        const LIST = 1 << 2;
        const MANAGE = 1 << 3;
        const EXECUTE = 1 << 4;
        const EXECUTE_SUID = 1 << 5;
        const ACCESS_DEVICES = 1 << 6;
        const LANDLOCK_EXECUTE = 1 << 7;
        const LANDLOCK_WRITE_FILE = 1 << 8;
        const LANDLOCK_READ_FILE = 1 << 9;
        const LANDLOCK_READ_DIR = 1 << 10;
        const LANDLOCK_REMOVE_DIR = 1 << 11;
        const LANDLOCK_REMOVE_FILE = 1 << 12;
        const LANDLOCK_MAKE_CHAR = 1 << 13;
        const LANDLOCK_MAKE_DIR = 1 << 14;
        const LANDLOCK_MAKE_REG = 1 << 15;
        const LANDLOCK_MAKE_SOCK = 1 << 16;
        const LANDLOCK_MAKE_FIFO = 1 << 17;
        const LANDLOCK_MAKE_BLOCK = 1 << 18;
        const LANDLOCK_MAKE_SYM = 1 << 19;
        const LANDLOCK_REFER = 1 << 20;
        const LANDLOCK_MAYBE_REFER = 1 << 21;
        const LANDLOCK_TRUNCATE = 1 << 22;
        const LANDLOCK_MAYBE_TRUNCATE = 1 << 23;
        const LANDLOCK_IOCTL_DEV = 1 << 24;
        const LANDLOCK_MAYBE_IOCTL_DEV = 1 << 25;
        const UNRESTRICTED = 1 << 26;
    }
}
impl FromStr for FsAccess {
    type Err = ParseFsAccessError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        s.split(',')
            .filter_map(|a| match a {
                "" => None,
                "read" | "r" => Some(Ok(FsAccess::READ)),
                "write" | "w" => Some(Ok(FsAccess::WRITE)),
                "list" | "l" => Some(Ok(FsAccess::LIST)),
                "manage" | "m" => Some(Ok(FsAccess::MANAGE)),
                "execute" | "x" => Some(Ok(FsAccess::EXECUTE)),
                "execute-suid" => Some(Ok(FsAccess::EXECUTE_SUID)),
                "access-devices" => Some(Ok(FsAccess::ACCESS_DEVICES)),
                "landlock-execute" => Some(Ok(FsAccess::LANDLOCK_EXECUTE)),
                "landlock-write-file" => Some(Ok(FsAccess::LANDLOCK_WRITE_FILE)),
                "landlock-read-file" => Some(Ok(FsAccess::LANDLOCK_READ_FILE)),
                "landlock-read-dir" => Some(Ok(FsAccess::LANDLOCK_READ_DIR)),
                "landlock-remove-dir" => Some(Ok(FsAccess::LANDLOCK_REMOVE_DIR)),
                "landlock-remove-file" => Some(Ok(FsAccess::LANDLOCK_REMOVE_FILE)),
                "landlock-make-char" => Some(Ok(FsAccess::LANDLOCK_MAKE_CHAR)),
                "landlock-make-dir" => Some(Ok(FsAccess::LANDLOCK_MAKE_DIR)),
                "landlock-make-reg" => Some(Ok(FsAccess::LANDLOCK_MAKE_REG)),
                "landlock-make-sock" => Some(Ok(FsAccess::LANDLOCK_MAKE_SOCK)),
                "landlock-make-fifo" => Some(Ok(FsAccess::LANDLOCK_MAKE_FIFO)),
                "landlock-make-block" => Some(Ok(FsAccess::LANDLOCK_MAKE_BLOCK)),
                "landlock-make-sym" => Some(Ok(FsAccess::LANDLOCK_MAKE_SYM)),
                "landlock-refer" => Some(Ok(FsAccess::LANDLOCK_REFER)),
                "landlock-maybe-refer" => Some(Ok(FsAccess::LANDLOCK_MAYBE_REFER)),
                "landlock-truncate" => Some(Ok(FsAccess::LANDLOCK_TRUNCATE)),
                "landlock-maybe-truncate" => Some(Ok(FsAccess::LANDLOCK_MAYBE_TRUNCATE)),
                "landlock-ioctl-dev" => Some(Ok(FsAccess::LANDLOCK_IOCTL_DEV)),
                "landlock-maybe-ioctl-dev" => Some(Ok(FsAccess::LANDLOCK_MAYBE_IOCTL_DEV)),
                "unrestricted" => Some(Ok(FsAccess::UNRESTRICTED)),
                _ => Some(Err(ParseFsAccessError(a.to_string()))),
            })
            .collect()
    }
}
#[derive(Debug, thiserror::Error)]
#[error("bad fs-access `{0}': unknown")]
pub struct ParseFsAccessError(String);

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[non_exhaustive]
pub enum TcpPort {
    All,
    Port(u16),
}
impl FromStr for TcpPort {
    type Err = ParseTcpPortError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s == "all" {
            Ok(Self::All)
        } else {
            Ok(Self::Port(u16::from_str(s)?))
        }
    }
}
#[derive(Debug, thiserror::Error)]
#[error("bad tcp port `{0}': {0}")]
pub struct ParseTcpPortError(#[from] ParseIntError);

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[non_exhaustive]
pub enum Connectivity {
    #[default]
    None,
    Pasta,
    Host,
    /// Not yet implemented!
    Tor,
}
impl FromStr for Connectivity {
    type Err = ParseConnectivityError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "pasta" => Ok(Self::Pasta),
            _ => Err(ParseConnectivityError(s.to_string())),
        }
    }
}
#[derive(Debug, thiserror::Error)]
#[error("bad connectivity `{0}': unknown")]
pub struct ParseConnectivityError(String);

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[non_exhaustive]
pub enum Socket {
    Unix,
    Ip,
    IpTcp,
    IpUdp,
    Ip4,
    Ip4Tcp,
    Ip4Udp,
    Ip6,
    Ip6Tcp,
    Ip6Udp,
    Netlink,
    NetlinkGeneric,
    NetlinkRoute,
    NetlinkSockDiag,
}
impl FromStr for Socket {
    type Err = ParseSocketError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "unix" => Ok(Self::Unix),
            "ip" => Ok(Self::Ip),
            "ip/tcp" => Ok(Self::IpTcp),
            "ip/udp" => Ok(Self::IpUdp),
            "ip4" => Ok(Self::Ip4),
            "ip4/tcp" => Ok(Self::Ip4Tcp),
            "ip4/udp" => Ok(Self::Ip4Udp),
            "ip6" => Ok(Self::Ip6),
            "ip6/tcp" => Ok(Self::Ip6Tcp),
            "ip6/udp" => Ok(Self::Ip6Udp),
            "netlink" => Ok(Self::Netlink),
            "netlink/generic" => Ok(Self::NetlinkGeneric),
            "netlink/route" => Ok(Self::NetlinkRoute),
            "netlink/sockdiag" => Ok(Self::NetlinkSockDiag),
            _ => Err(ParseSocketError(s.to_string())),
        }
    }
}
#[derive(Debug, thiserror::Error)]
#[error("bad socket `{0}': unknown")]
pub struct ParseSocketError(String);

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct DBusFilterRule {
    pub method: Option<String>,
    pub path: Option<String>,
    pub call: bool,
    pub broadcast: bool,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
#[non_exhaustive]
pub enum Namespace {
    Cgroup,
    Ipc,
    Mnt,
    Net,
    Pid,
    Time,
    User,
    Uts,
}
impl FromStr for Namespace {
    type Err = ParseNamespaceError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "cgroup" => Ok(Self::Cgroup),
            "ipc" => Ok(Self::Ipc),
            "mnt" => Ok(Self::Mnt),
            "net" => Ok(Self::Net),
            "pid" => Ok(Self::Pid),
            "time" => Ok(Self::Time),
            "user" => Ok(Self::User),
            "uts" => Ok(Self::Uts),
            _ => Err(ParseNamespaceError(s.to_string())),
        }
    }
}
#[derive(Debug, thiserror::Error)]
#[error("bad namespace `{0}': unknown")]
pub struct ParseNamespaceError(String);

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum EnvAction {
    Unset,
    Keep,
    Set(String),
}

/*
 * Copyright © 2024,2025 The crabjail Authors
 *
 * This file is part of crabjail
 *
 * crabjail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabjail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#[macro_use]
mod utils;

mod app_id;
mod crablock_cmd;
mod error;
mod expand_path;
mod jail;
mod jail_file;

pub mod config;
pub mod dirs;
pub mod dynamic_permissions;
pub mod permissions;

pub use app_id::{app_id, set_app_id};
pub use error::Error;
pub use jail::Jail;
pub use jail_file::JailFile;

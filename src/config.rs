/*
 * Copyright © 2024,2025 The crabjail Authors
 *
 * This file is part of crabjail
 *
 * crabjail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabjail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::collections::HashMap;
use std::fs;
use std::fs::File;
use std::iter;
use std::path::Path;
use std::sync::OnceLock;

use lnix::std_suppl::io::ErrorKindExt;
use serde::de::DeserializeOwned;

pub(crate) mod syscall_groups;

pub(crate) fn config_dirs() -> impl Iterator<Item = &'static Path> {
    // development stub, move to /etc/crabjail, ~/.config/crabjail for release
    iter::once(Path::new("./config"))
}

#[expect(dead_code, reason = "Possible future use")]
pub(crate) fn open_config_files(conf_name: &str) -> impl Iterator<Item = File> {
    config_dirs().filter_map(
        move |config_dir| match File::open(config_dir.join(conf_name)) {
            Ok(file) => Some(file),
            Err(err) => {
                if !err.kind().is_not_found() {
                    log::warn!(
                        "Opening {} in {} failed with {}",
                        conf_name,
                        config_dir.display(),
                        err
                    );
                }
                None
            }
        },
    )
}

#[expect(dead_code, reason = "Possible future use")]
pub(crate) fn read_config_files(conf_name: &str) -> impl Iterator<Item = String> {
    config_dirs().filter_map(move |config_dir| {
        match fs::read_to_string(config_dir.join(conf_name)) {
            Ok(file) => Some(file),
            Err(err) => {
                if !err.kind().is_not_found() {
                    log::warn!(
                        "Reading {} in {} failed with {}",
                        conf_name,
                        config_dir.display(),
                        err
                    );
                }
                None
            }
        }
    })
}

pub(crate) fn parse_config_files<T: DeserializeOwned>(conf_name: &str) -> impl Iterator<Item = T> {
    config_dirs()
        .filter_map(move |config_dir| {
            let conf_path = config_dir.join(conf_name);
            match fs::read_to_string(&conf_path) {
                Ok(s) => Some((conf_path, s)),
                Err(err) => {
                    if !err.kind().is_not_found() {
                        log::warn!("Reading {} failed with {}", conf_path.display(), err);
                    }
                    None
                }
            }
        })
        .filter_map(|(conf_path, s)| match toml::from_str(&s) {
            Ok(t) => Some(t),
            Err(err) => {
                log::warn!("Parsing {} failed with {}", conf_path.display(), err);
                None
            }
        })
}

pub(crate) fn get_merged_config_files<T, U>(conf_name: &str) -> T
where
    T: DeserializeOwned + FromIterator<U> + IntoIterator<Item = U>,
{
    parse_config_files(conf_name)
        .flat_map(|t: T| t.into_iter())
        .collect()
}

pub fn get_config_section(section: &str) -> Option<&HashMap<String, toml::Value>> {
    static CONFIG: OnceLock<HashMap<String, HashMap<String, toml::Value>>> = OnceLock::new();

    if let Some(config) = CONFIG.get() {
        return config.get(section);
    }

    let _ = CONFIG.set(get_merged_config_files("config.toml"));

    get_config_section(section)
}

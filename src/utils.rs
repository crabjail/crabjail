/*
 * Copyright © 2021,2022,2024 The crabjail Authors
 *
 * This file is part of crabjail
 *
 * crabjail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabjail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

//! Module for various programming helpers not directly related to crabjail.

use std::fs::read_to_string;
use std::time::SystemTime;

/// Time an execution
///
/// You can specify a number of repetition `timed!(N = 32 => ...)`
/// or use the default number of repetitions `timed!(...)`.
#[expect(unused_macros, reason = "move to crabutils!")]
macro_rules! timed {
    (N = $N:expr => $( $tt:tt )* ) => {{
        let start_time = ::std::time::Instant::now();
        let ret = { $( $tt )* };
        for _ in 1..$N {
            { $( $tt )* };
        }
        let avg_dur = start_time.elapsed() / $N;
        ::log::info!(
            "timed! {}:{}:{} - {}s {}ms {}µs {}ns (N={}). {}s",
            file!(), line!(), column!(),
            avg_dur.as_secs(),
            avg_dur.subsec_millis(),
            avg_dur.subsec_micros() - avg_dur.subsec_millis() * 1000,
            avg_dur.subsec_nanos() - avg_dur.subsec_micros() * 1000,
            $N,
        );
        ::log::debug!("Code:\n{}", stringify!( $( $tt )* ));
        ret
    }};
    ( $( $tt:tt )* ) => {
        timed!(N = 16 => $( $tt )* )
    };
}

/// Alias for `timed!(N = 1 => ...)`
#[expect(unused_macros, reason = "move to crabutils!")]
macro_rules! timed_once {
    ( $( $tt:tt )* ) => {
        timed!(N = 1 => $( $tt )* )
    };
}

/// `const` assertions
#[expect(unused_macros, reason = "move to crabutils!")]
macro_rules! const_assert {
    ( $cond:expr $(,)? ) => {
        const _: () = assert!($cond);
    };
    ( $cond:expr, $( $arg:tt )+ ) => {
        const _: () = assert!($cond, $( $arg )+);
    }
}

/// Get a new v4 (random) UUID
pub(crate) fn uuid4() -> String {
    let mut uuid = read_to_string("/proc/sys/kernel/random/uuid").unwrap();
    // Remove linebreak; usually you would use .trim().to_string()
    // (or an in-place implementation), but since we know the exact
    // format, we can use .pop() which is much faster.
    uuid.pop();
    uuid
}

/// Seconds since `1970-01-01 00:00:00 UTC`
#[expect(dead_code, reason = "move to crabutils!")]
pub(crate) fn unix_now() -> u64 {
    SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap()
        .as_secs()
}

// /// Log the `Debug` representation of `t` via [`log::trace`]
// pub(crate) fn log_t<T: Debug + ?Sized>(t: &T) {
//     if log::max_level() >= log::LevelFilter::Debug {
//         for line in format!("{:#?}", t).lines() {
//             log::trace!("{}", line);
//         }
//     }
// }

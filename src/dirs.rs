/*
 * Copyright © 2024,2025 The crabjail Authors
 *
 * This file is part of crabjail
 *
 * crabjail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabjail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

// FIXME: Merge copy of this file in crablock into a new crate (crabdirs or crabutils::dirs?).
// Related topic: expand_path.rs

use std::env;
use std::fs;
use std::path::{Path, PathBuf};
use std::sync::LazyLock;

/// Returns the path of the user's home directory.
pub fn try_home_dir() -> Option<&'static Path> {
    static HOME_DIR: LazyLock<Option<PathBuf>> =
        LazyLock::new(|| Some(PathBuf::from(env::var_os("HOME").expect("$HOME"))));
    HOME_DIR.as_deref()
}

/// Returns the path of the user's home directory.
///
/// # Panics
///
/// This function panics if `$HOME` is not set.
pub fn home_dir() -> &'static Path {
    try_home_dir().expect("get home directory")
}

/// Returns ...
///
/// If `$PATH` is not set, an empty slice is returned.
pub fn executable_dirs() -> &'static [PathBuf] {
    static PATH: LazyLock<Vec<PathBuf>> =
        LazyLock::new(|| env::split_paths(&env::var_os("PATH").unwrap_or_default()).collect());
    &PATH
}

/// Returns the directory for temporary files.
///
/// `$TMPDIR` if set, `/tmp` otherwise.
pub fn tmp_dir() -> &'static Path {
    static TMPDIR: LazyLock<PathBuf> = LazyLock::new(|| {
        if let Some(tmpdir) = env::var_os("TMPDIR") {
            PathBuf::from(tmpdir)
        } else {
            PathBuf::from("/tmp")
        }
    });
    &TMPDIR
}

/// Returns the path to the user's cache directory.
///
/// # Panics
///
/// This function panics if [`home_dir()`] panics.
pub fn xdg_cache_home() -> &'static Path {
    static XDG_CACHE_HOME: LazyLock<PathBuf> = LazyLock::new(|| {
        if let Some(xdg_cache_home) = env::var_os("XDG_CACHE_HOME") {
            PathBuf::from(xdg_cache_home)
        } else {
            home_dir().join(".cache")
        }
    });
    &XDG_CACHE_HOME
}

/// Returns the path to the user's config directory.
///
/// # Panics
///
/// This function panics if [`home_dir()`] panics.
pub fn xdg_config_home() -> &'static Path {
    static XDG_CONFIG_HOME: LazyLock<PathBuf> = LazyLock::new(|| {
        if let Some(xdg_config_home) = env::var_os("XDG_CONFIG_HOME") {
            PathBuf::from(xdg_config_home)
        } else {
            home_dir().join(".config")
        }
    });
    &XDG_CONFIG_HOME
}

/// Returns the path to the user's data directory.
///
/// # Panics
///
/// This function panics if [`home_dir()`] panics.
pub fn xdg_data_home() -> &'static Path {
    static XDG_DATA_HOME: LazyLock<PathBuf> = LazyLock::new(|| {
        if let Some(xdg_data_home) = env::var_os("XDG_DATA_HOME") {
            PathBuf::from(xdg_data_home)
        } else {
            home_dir().join(".local/share")
        }
    });
    &XDG_DATA_HOME
}

/// Returns the path to the user's state directory.
///
/// # Panics
///
/// This function panics if [`home_dir()`] panics.
pub fn xdg_state_home() -> &'static Path {
    static XDG_STATE_HOME: LazyLock<PathBuf> = LazyLock::new(|| {
        if let Some(xdg_state_home) = env::var_os("XDG_STATE_HOME") {
            PathBuf::from(xdg_state_home)
        } else {
            home_dir().join(".local/state")
        }
    });
    &XDG_STATE_HOME
}

/// Returns the path to the user's runtime directory.
///
/// # Panics
///
/// This function panics if `$XDG_RUNTIME_DIR` is not set.
pub fn xdg_runtime_dir() -> &'static Path {
    static XDG_RUNTIME_DIR: LazyLock<PathBuf> =
        LazyLock::new(|| PathBuf::from(env::var_os("XDG_RUNTIME_DIR").expect("$XDG_RUNTIME_DIR")));
    &XDG_RUNTIME_DIR
}

/// Returns the paths to the system's config directories.
///
/// If `$XDG_CONFIG_DIRS` is not set, an empty slice is returned.
pub fn xdg_config_dirs() -> &'static [PathBuf] {
    static XDG_CONFIG_DIRS: LazyLock<Vec<PathBuf>> = LazyLock::new(|| {
        env::split_paths(&env::var_os("XDG_CONFIG_DIRS").unwrap_or_default()).collect()
    });
    &XDG_CONFIG_DIRS
}

/// Returns the paths to the system's data directories.
///
/// If `$XDG_DATA_DIRS` is not set, an empty slice is returned.
pub fn xdg_data_dirs() -> &'static [PathBuf] {
    static XDG_DATA_DIRS: LazyLock<Vec<PathBuf>> = LazyLock::new(|| {
        env::split_paths(&env::var_os("XDG_DATA_DIRS").unwrap_or_default()).collect()
    });
    &XDG_DATA_DIRS
}

#[derive(Debug)]
pub struct UserDirs {
    pub desktop: Option<PathBuf>,
    pub documents: Option<PathBuf>,
    pub download: Option<PathBuf>,
    pub music: Option<PathBuf>,
    pub pictures: Option<PathBuf>,
    pub publicshare: Option<PathBuf>,
    pub templates: Option<PathBuf>,
    pub videos: Option<PathBuf>,
}

impl UserDirs {
    pub fn get() -> &'static Self {
        static USER_DIRS: LazyLock<UserDirs> = LazyLock::new(UserDirs::from_config);
        &USER_DIRS
    }

    fn from_config() -> Self {
        let mut user_dirs = Self {
            desktop: None,
            documents: None,
            download: None,
            music: None,
            pictures: None,
            publicshare: None,
            templates: None,
            videos: None,
        };

        let Ok(user_dirs_list) = fs::read_to_string(xdg_config_home().join("user-dirs.dirs"))
        else {
            return user_dirs;
        };
        for line in user_dirs_list.lines() {
            match line.trim().split_once('=') {
                Some(("XDG_DESKTOP_DIR", path)) => user_dirs.desktop = Self::expand(path),
                Some(("XDG_DOCUMENTS_DIR", path)) => user_dirs.documents = Self::expand(path),
                Some(("XDG_DOWNLOAD_DIR", path)) => user_dirs.download = Self::expand(path),
                Some(("XDG_MUSIC_DIR", path)) => user_dirs.music = Self::expand(path),
                Some(("XDG_PICTURES_DIR", path)) => user_dirs.pictures = Self::expand(path),
                Some(("XDG_PUBLICSHARE_DIR", path)) => {
                    user_dirs.publicshare = Self::expand(path);
                }
                Some(("XDG_TEMPLATES_DIR", path)) => user_dirs.templates = Self::expand(path),
                Some(("XDG_VIDEOS_DIR", path)) => user_dirs.videos = Self::expand(path),
                // Comment, empty, invalid or unknown.
                _ => (),
            }
        }
        user_dirs
    }

    fn expand(path: &str) -> Option<PathBuf> {
        /* Remove leading/trailing quotes. */
        let path = path.trim_matches('"');

        if path.starts_with('/') {
            /* If path is absolute, return it. */
            Some(PathBuf::from(path))
        } else if let Some(path) = path.strip_prefix("$HOME/") {
            /* path may start with the users home directory. */
            let path = home_dir().join(path);
            if path == home_dir() {
                /* We do not consider a user-dir equal to the user's
                 * home directory to be a valid user-dir. */
                None
            } else {
                Some(path)
            }
        } else {
            /* path is neither absolute
             * nor starts with the user's home directory. */
            None
        }
    }
}

/*
 * Copyright © 2024,2025 The crabjail Authors
 *
 * This file is part of crabjail
 *
 * crabjail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabjail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::borrow::Cow;
use std::sync::OnceLock;

static APP_ID: OnceLock<Cow<'static, str>> = OnceLock::new();

pub fn app_id() -> &'static str {
    APP_ID.get().expect("app_id must be initialized")
}

pub fn set_app_id<S: Into<Cow<'static, str>>>(app_id: S) {
    if APP_ID.set(app_id.into()).is_err() {
        panic!("Failed to set_app_id.");
    }
}

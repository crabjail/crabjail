/*
 * Copyright © 2024,2025 The crabjail Authors
 *
 * This file is part of crabjail
 *
 * crabjail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabjail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::collections::HashMap;
use std::path::PathBuf;

use compact_str::CompactString;
use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub(super) struct Root {
    pub(super) jail: Jail,

    #[serde(flatten)]
    pub(super) third_party_sections: HashMap<String, HashMap<String, toml::Value>>,
}
impl Root {
    pub(super) fn from_toml(s: &str) -> Result<Self, toml::de::Error> {
        toml::from_str(s)
    }
}

#[derive(Debug, Default, Deserialize)]
#[serde(default)]
#[serde(deny_unknown_fields)]
#[serde(rename_all = "kebab-case")]
pub(super) struct Jail {
    pub(super) name: CompactString,
    pub(super) compat: u8,

    pub(super) permissions: Vec<JailPermissionsItem>,
    pub(super) paths: Vec<JailPathsItem>,
    pub(super) devices: Vec<JailDevicesItem>,
    pub(super) network: Vec<JailNetworkItem>,
    pub(super) session_bus: Vec<JailDBusItem>,
    pub(super) system_bus: Vec<JailDBusItem>,
    pub(super) syscalls: Vec<CompactString>,
    pub(super) namespaces_limits: JailNamespacesLimits,

    pub(super) default_environment: Option<bool>,
    pub(super) environment: Vec<String>,
}

#[derive(Debug, Deserialize)]
#[serde(untagged)]
pub(super) enum JailPermissionsItem {
    Simple(CompactString),
}

#[derive(Debug, Deserialize)]
#[serde(untagged)]
pub(super) enum JailPathsItem {
    FsPath {
        path: PathBuf,
        action: CompactString,
        access: Vec<CompactString>,
    },
}

#[derive(Debug, Deserialize)]
#[serde(untagged)]
pub(super) enum JailDevicesItem {
    Simple(CompactString),
}

#[derive(Debug, Deserialize)]
#[serde(untagged)]
#[serde(rename_all = "kebab-case")]
pub(super) enum JailNetworkItem {
    Simple(CompactString),
    BindTcp { bind_tcp: u16 },
    ConnectTcp { connect_tcp: u16 },
    Connectivity { connectivity: CompactString },
    Socket { socket: CompactString },
}

#[derive(Debug, Deserialize)]
#[serde(untagged)]
#[serde(rename_all = "kebab-case")]
pub(super) enum JailDBusItem {
    Basic {
        name: String,
        policy: JailDBusItemPolicyValue,
    },
    Rule {
        name: String,
        policy: JailDBusItemPolicyValue,
        method: Option<String>,
        path: Option<String>,
        call: bool,
        broadcast: bool,
    },
    Portal {
        portal: CompactString,
    },
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
#[serde(rename_all = "lowercase")]
pub(super) enum JailDBusItemPolicyValue {
    Own,
    Talk,
    See,
}

#[derive(Debug, Default, Deserialize)]
pub(super) struct JailNamespacesLimits {
    pub(super) cgroup: Option<JailNamespacesLimitsValue>,
    pub(super) ipc: Option<JailNamespacesLimitsValue>,
    pub(super) mnt: Option<JailNamespacesLimitsValue>,
    pub(super) net: Option<JailNamespacesLimitsValue>,
    pub(super) pid: Option<JailNamespacesLimitsValue>,
    pub(super) time: Option<JailNamespacesLimitsValue>,
    pub(super) user: Option<JailNamespacesLimitsValue>,
    pub(super) uts: Option<JailNamespacesLimitsValue>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
#[serde(try_from = "toml::Value")]
pub(super) enum JailNamespacesLimitsValue {
    Count(u8),
    Inf,
}
impl TryFrom<toml::Value> for JailNamespacesLimitsValue {
    type Error = String;

    fn try_from(value: toml::Value) -> Result<Self, Self::Error> {
        match value {
            toml::Value::Integer(count) if count >= 0 => Ok(Self::Count(count as u8)),
            toml::Value::Float(f64::INFINITY) => Ok(Self::Inf),
            _ => Err(format!(
                "Expected non-negative integer or `inf', got {}",
                value.type_str()
            )),
        }
    }
}

/*
 * Copyright © 2024,2025 The crabjail Authors
 *
 * This file is part of crabjail
 *
 * crabjail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabjail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::collections::HashMap;
use std::sync::Arc;

use compact_str::CompactString;
use regex::Regex;

use crate::Error;
use crate::crablock_cmd::CrablockCmd;
use crate::dirs::{xdg_data_home, xdg_runtime_dir};
use crate::permissions::{EnvAction, Permission, TcpPort};
use crate::utils::uuid4;

#[macro_use]
mod clock_arg_macros;

mod dbus;
mod filesystem;
mod namespaces_limits;
mod network;
mod syscall_filter;

#[derive(Debug, Clone)]
#[non_exhaustive]
pub struct Jail {
    name: CompactString,

    filesystem: filesystem::Filesystem,
    network: network::Network,
    syscall_filter: syscall_filter::SyscallFilter,
    namespaces_limits: namespaces_limits::NamespacesLimits,
    session_bus: dbus::SessionBusProxy,
    system_bus: dbus::SystemBusProxy,

    allow_execve_null: bool,
    memory_allow_write_execute: bool,
    // TODO: ld_preload_libhardened_malloc: bool,
    //
    environment: HashMap<String, EnvAction>,

    raw_crablock_arguments: Vec<String>,
}
impl Jail {
    pub fn new<S: Into<CompactString>>(name: S) -> Result<Self, Error> {
        Self::new_impl(name.into())
    }

    fn new_impl(name: CompactString) -> Result<Self, Error> {
        const NAME_REGEX: &str = r#"^[A-Za-z_][A-Za-z0-9_.-]+$"#;

        if !Regex::new(NAME_REGEX).unwrap().is_match(&name) {
            unimplemented!()
        }

        Ok(Self {
            name,

            filesystem: filesystem::Filesystem::default(),
            network: network::Network::default(),
            session_bus: dbus::SessionBusProxy::default(),
            system_bus: dbus::SystemBusProxy::default(),
            syscall_filter: syscall_filter::SyscallFilter::default(),
            namespaces_limits: namespaces_limits::NamespacesLimits::default(),

            allow_execve_null: false,
            memory_allow_write_execute: false,
            // TODO: ld_preload_libhardened_malloc: true,
            //
            environment: HashMap::default(),

            raw_crablock_arguments: Vec::new(),
        })
    }

    pub fn add(&mut self, permission: Permission) -> Result<(), Error> {
        match permission {
            Permission::FsPath { path, policy } => {
                self.filesystem.add(path, policy.action, policy.access);
            }
            Permission::FsFullProc => self.filesystem.set_full_proc(),
            Permission::NetConnectivity(connectivity) => {
                self.network.set_connectivity(connectivity);
            }
            Permission::NetSocket(socket) => self.network.add_socket(socket),
            Permission::NetBindTcp(TcpPort::All) => self.network.set_bind_tcp_all(),
            Permission::NetBindTcp(TcpPort::Port(port)) => self.network.add_bind_tcp(port),
            Permission::NetConnectTcp(TcpPort::All) => self.network.set_connect_tcp_all(),
            Permission::NetConnectTcp(TcpPort::Port(port)) => self.network.add_connect_tcp(port),
            Permission::Syscall(syscall) => self.syscall_filter.allow(syscall),
            Permission::NamespacesLimit(ns, lmt) => self.namespaces_limits.set_limit(ns, lmt),
            Permission::MemoryAllowWriteExecute => self.memory_allow_write_execute = true,
            Permission::SessionBusOwn { name } => self.session_bus.own_name(name),
            Permission::SessionBusTalk { name } => self.session_bus.talk_name(name),
            Permission::SessionBusTalkRestricted { name, rule } => {
                self.session_bus.talk_name_restricted(name, rule);
            }
            Permission::SessionBusSee { name } => self.session_bus.see_name(name),
            Permission::SystemBusOwn { name } => self.system_bus.own_name(name),
            Permission::SystemBusTalk { name } => self.system_bus.talk_name(name),
            Permission::SystemBusTalkRestricted { name, rule } => {
                self.system_bus.talk_name_restricted(name, rule);
            }
            Permission::SystemBusSee { name } => self.system_bus.see_name(name),
        }

        Ok(())
    }

    pub fn env<S: Into<String>>(&mut self, name: S, action: EnvAction) {
        self.env_impl(name.into(), action);
    }

    fn env_impl(&mut self, name: String, action: EnvAction) {
        const PROTECTED_ENVIRONMENT_VARIABLES: &[&str] = &[
            "container",
            "DBUS_SESSION_BUS_ADDRESS",
            "DBUS_SYSTEM_BUS_ADDRESS",
        ];

        if PROTECTED_ENVIRONMENT_VARIABLES.contains(&&*name) {
            return;
        }

        debug_assert!(!name.contains('='));
        if self.environment.insert(name, action).is_some() {
            unimplemented!();
        }
    }

    pub fn append_raw_crablock_args<I: IntoIterator<Item = S>, S: Into<String>>(
        &mut self,
        raw_args: I,
    ) {
        self.raw_crablock_arguments
            .extend(raw_args.into_iter().map(Into::into));
    }

    pub fn crablock_cmd(&self) -> CrablockCmd<'_> {
        let data_home = xdg_data_home().join(crate::app_id());
        let jail_instance_runtime_dir = Arc::new(
            xdg_runtime_dir()
                .join("app")
                .join(crate::app_id())
                .join("jail")
                .join(&self.name)
                .join(&uuid4()[..8]),
        );

        let mut clockcmd = CrablockCmd::new(Arc::clone(&jail_instance_runtime_dir));

        clockcmd.argv.append(
            [
                clock_arg!["--setenv", "container=crabjail"],
                clock_arg![
                    "--crablock-data-home",
                    data_home.to_str().unwrap().to_string(),
                ],
                clock_arg![
                    "--crablock-runtime-dir",
                    jail_instance_runtime_dir.to_str().unwrap().to_string(),
                ],
                clock_arg!["--hostname", "localhost"],
                clock_arg!["--unshare", "all"],
                clock_arg!["--capabilities", "none"],
                // TODO --choom, --nice + CGroups!
            ]
            .as_flattened(),
        );
        clockcmd.argv.append(&clock_arg!["--no-new-privs"]);
        clockcmd.argv.append(&clock_arg!["--strict-mitigations"]);
        clockcmd
            .argv
            .append(&clock_arg!["--landlock-scope-abstract-unix-socket"]);
        clockcmd.argv.append(&clock_arg!["--landlock-scope-signal"]);

        if !self.allow_execve_null {
            clockcmd
                .argv
                .append(&clock_arg!["--seccomp-deny-execve-null"]);
        }
        if !self.memory_allow_write_execute {
            clockcmd.argv.append(&clock_arg!["--mdwe-refuse-exec-gain"]);
            clockcmd.argv.append(&clock_arg!["--seccomp-memfd-noexec"]);
        }

        self.filesystem.to_crablock_argv(&mut clockcmd.argv);
        self.network.to_crablock_argv(&mut clockcmd.argv);
        self.syscall_filter.to_crablock_argv(&mut clockcmd.argv);
        self.namespaces_limits.to_crablock_argv(&mut clockcmd.argv);
        self.session_bus
            .to_crablock_argv(&mut clockcmd.argv, &jail_instance_runtime_dir);
        self.system_bus
            .to_crablock_argv(&mut clockcmd.argv, &jail_instance_runtime_dir);

        clockcmd.argv.append(&clock_arg!["--clearenv"]);
        clockcmd.argv.reserve(self.environment.len() * 2);
        for (name, action) in &self.environment {
            match action {
                EnvAction::Unset => clockcmd.argv.append(&clock_arg!["--unsetenv", name]),
                EnvAction::Keep => clockcmd.argv.append(&clock_arg!["--keepenv", name]),
                EnvAction::Set(value) => clockcmd
                    .argv
                    .append(&clock_arg!["--setenv", format!("{name}={value}")]),
            }
        }

        clockcmd
            .argv
            .extend(self.raw_crablock_arguments.iter().map(Into::into));

        clockcmd
    }
}

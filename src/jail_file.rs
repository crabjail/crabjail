/*
 * Copyright © 2024,2025 The crabjail Authors
 *
 * This file is part of crabjail
 *
 * crabjail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crabjail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::borrow::Cow;
use std::collections::HashMap;
use std::fs;
use std::path::Path;
use std::str::FromStr;

use crate::config::config_dirs;
use crate::dirs::home_dir;
use crate::dynamic_permissions::DYNAMIC_PERMISSIONS;
use crate::error::InnerError;
use crate::expand_path::expand_path;
use crate::permissions::*;
use crate::{Error, Jail};

mod toml_model;

#[derive(Debug)]
pub struct JailFile(toml_model::Root);
impl JailFile {
    pub fn open(name_or_path: &str) -> Result<Self, Error> {
        (|| -> Result<Self, InnerError> {
            let path = if name_or_path.contains('/') {
                Cow::Borrowed(Path::new(name_or_path))
            } else {
                Cow::Owned(
                    config_dirs()
                        .map(|dir| dir.join("jails").join(format!("{name_or_path}.jail")))
                        .find(|path| path.exists())
                        .unwrap_or_else(|| {
                            config_dirs()
                                .next()
                                .unwrap()
                                .join("jails")
                                .join(format!("{name_or_path}.jail"))
                        }),
                )
            };
            let string = fs::read_to_string(&*path)?;

            Ok(Self(toml_model::Root::from_toml(&string)?))
        })()
        .map_err(From::from)
    }

    pub fn get_third_party_section(&self, section: &str) -> Option<&HashMap<String, toml::Value>> {
        self.0.third_party_sections.get(section)
    }

    pub fn to_jail(&self) -> Result<Jail, Error> {
        self.to_jail_impl().map_err(From::from)
    }

    // TODO: rayon
    fn to_jail_impl(&self) -> Result<Jail, InnerError> {
        if self.0.jail.compat != 0 {
            unimplemented!()
        }

        DYNAMIC_PERMISSIONS.write().expect("fixme").init();

        let mut jail = Jail::new(self.0.jail.name.clone())?;

        for item in &self.0.jail.permissions {
            match item {
                toml_model::JailPermissionsItem::Simple(s) => {
                    DYNAMIC_PERMISSIONS
                        .read()
                        .expect("fixme")
                        .apply_permission(&**s, &mut jail)
                        .map_err(|err| {
                            InnerError::simple_with_boxed_source(
                                format!("fixme apply_permission {s}"),
                                err,
                            )
                        })?;
                }
            }
        }

        for item in &self.0.jail.paths {
            match item {
                toml_model::JailPathsItem::FsPath {
                    path,
                    action,
                    access,
                } => jail.add(Permission::FsPath {
                    path: expand_path(path).into_owned(),
                    policy: FsPolicy {
                        action: FsAction::from_str(action)?,
                        access: access
                            .iter()
                            .map(|s| FsAccess::from_str(s))
                            .collect::<Result<_, _>>()?,
                    },
                })?,
            }
        }

        for item in &self.0.jail.devices {
            match item {
                toml_model::JailDevicesItem::Simple(s) => {
                    if let Some(s) = s.strip_prefix('@') {
                        DYNAMIC_PERMISSIONS
                            .read()
                            .expect("fixme")
                            .apply_device_group(s, &mut jail)
                            .map_err(|err| {
                                InnerError::simple_with_boxed_source(
                                    format!("fixme apply_device_group {s}"),
                                    err,
                                )
                            })?;
                    } else {
                        unimplemented!();
                    }
                }
            }
        }

        for item in &self.0.jail.network {
            match item {
                toml_model::JailNetworkItem::Simple(s) => {
                    DYNAMIC_PERMISSIONS
                        .read()
                        .expect("fixme")
                        .apply_network(&**s, &mut jail)
                        .map_err(|err| {
                            InnerError::simple_with_boxed_source(
                                format!("fixme apply_network {s}"),
                                err,
                            )
                        })?;
                }
                toml_model::JailNetworkItem::BindTcp { bind_tcp: port } => {
                    jail.add(Permission::NetBindTcp(TcpPort::Port(*port)))?;
                }
                toml_model::JailNetworkItem::ConnectTcp { connect_tcp: port } => {
                    jail.add(Permission::NetConnectTcp(TcpPort::Port(*port)))?;
                }
                toml_model::JailNetworkItem::Connectivity { connectivity } => {
                    jail.add(Permission::NetConnectivity(Connectivity::from_str(
                        connectivity,
                    )?))?;
                }
                toml_model::JailNetworkItem::Socket { socket } => {
                    jail.add(Permission::NetSocket(Socket::from_str(socket)?))?;
                }
            }
        }

        for item in &self.0.jail.session_bus {
            match item {
                toml_model::JailDBusItem::Basic { name, policy } => {
                    jail.add(match policy {
                        toml_model::JailDBusItemPolicyValue::Own => {
                            Permission::SessionBusOwn { name: name.clone() }
                        }
                        toml_model::JailDBusItemPolicyValue::Talk => {
                            Permission::SessionBusTalk { name: name.clone() }
                        }
                        toml_model::JailDBusItemPolicyValue::See => {
                            Permission::SessionBusSee { name: name.clone() }
                        }
                    })?;
                }
                toml_model::JailDBusItem::Rule {
                    name,
                    policy,
                    method,
                    path,
                    call,
                    broadcast,
                } => {
                    if *policy != toml_model::JailDBusItemPolicyValue::Talk {
                        unimplemented!();
                    }
                    jail.add(Permission::SessionBusTalkRestricted {
                        name: name.clone(),
                        rule: DBusFilterRule {
                            method: method.clone(),
                            path: path.clone(),
                            call: *call,
                            broadcast: *broadcast,
                        },
                    })?;
                }
                toml_model::JailDBusItem::Portal { portal } => {
                    DYNAMIC_PERMISSIONS
                        .read()
                        .expect("fixme")
                        .apply_portal(&**portal, &mut jail)
                        .map_err(|err| {
                            InnerError::simple_with_boxed_source(
                                format!("fixme apply_portal {portal}"),
                                err,
                            )
                        })?;
                }
            }
        }
        for item in &self.0.jail.system_bus {
            match item {
                toml_model::JailDBusItem::Basic { name, policy } => {
                    jail.add(match policy {
                        toml_model::JailDBusItemPolicyValue::Own => {
                            Permission::SystemBusOwn { name: name.clone() }
                        }
                        toml_model::JailDBusItemPolicyValue::Talk => {
                            Permission::SystemBusTalk { name: name.clone() }
                        }
                        toml_model::JailDBusItemPolicyValue::See => {
                            Permission::SystemBusSee { name: name.clone() }
                        }
                    })?;
                }
                toml_model::JailDBusItem::Rule {
                    name,
                    policy,
                    method,
                    path,
                    call,
                    broadcast,
                } => {
                    if *policy != toml_model::JailDBusItemPolicyValue::Talk {
                        unimplemented!();
                    }
                    jail.add(Permission::SystemBusTalkRestricted {
                        name: name.clone(),
                        rule: DBusFilterRule {
                            method: method.clone(),
                            path: path.clone(),
                            call: *call,
                            broadcast: *broadcast,
                        },
                    })?;
                }
                toml_model::JailDBusItem::Portal { .. } => unimplemented!(),
            }
        }

        for syscall in &self.0.jail.syscalls {
            jail.add(Permission::Syscall(syscall.clone()))?;
        }

        for (namespaces_limit, namespace) in [
            (self.0.jail.namespaces_limits.cgroup, Namespace::Cgroup),
            (self.0.jail.namespaces_limits.ipc, Namespace::Ipc),
            (self.0.jail.namespaces_limits.mnt, Namespace::Mnt),
            (self.0.jail.namespaces_limits.net, Namespace::Net),
            (self.0.jail.namespaces_limits.pid, Namespace::Pid),
            (self.0.jail.namespaces_limits.time, Namespace::Time),
            (self.0.jail.namespaces_limits.user, Namespace::User),
            (self.0.jail.namespaces_limits.uts, Namespace::Uts),
        ] {
            if let Some(limit) = namespaces_limit {
                jail.add(Permission::NamespacesLimit(
                    namespace,
                    match limit {
                        toml_model::JailNamespacesLimitsValue::Count(count) => Some(count),
                        toml_model::JailNamespacesLimitsValue::Inf => None,
                    },
                ))?;
            }
        }

        if self.0.jail.default_environment.unwrap_or(true) {
            for (name, action) in [
                (
                    "PATH",
                    EnvAction::Set(String::from(
                        "/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin",
                    )),
                ),
                ("SHELL", EnvAction::Set(String::from("/bin/bash"))),
                (
                    "HOME",
                    EnvAction::Set(String::from(home_dir().to_str().expect("utf-8"))),
                ),
                ("USER", EnvAction::Keep),
                ("USERNAME", EnvAction::Keep),
                ("LOGNAME", EnvAction::Keep),
                ("LANG", EnvAction::Keep),
                ("TERM", EnvAction::Keep),
                //
                ("XDG_CURRENT_DESKTOP", EnvAction::Keep),
                ("DESKTOP_SESSION", EnvAction::Keep),
                // FIXME: This is set by wayland dyn permission too
                //("XDG_SESSION_TYPE", EnvAction::Keep),
                ("XDG_SESSION_DESKTOP", EnvAction::Keep),
                //
                ("XDG_RUNTIME_DIR", EnvAction::Keep),
                //
                ("GIO_USE_VFS", EnvAction::Set(String::from("local"))),
                ("GSETTINGS_BACKEND", EnvAction::Set(String::from("keyfile"))),
                // # fs.private-volatile [r,w,landlock-make-reg] @{XDG_RUNTIME_DIR}/dconf
            ] {
                jail.env(name, action);
            }
        }

        for item in &self.0.jail.environment {
            if let Some((name, value)) = item.split_once('=') {
                jail.env(name, EnvAction::Set(value.to_string()));
            } else {
                jail.env(item, EnvAction::Keep);
            }
        }

        Ok(jail)
    }
}

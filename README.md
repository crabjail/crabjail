# crabjail

Linux sandboxing tool.

crabjail is the high-level counterpart to [crablock](https://codeberg.org/crabjail/crablock).

## Table Of Contents

TBD

## Motivation

TBW

## Features

TBW

## Installation

At the moment only x86_64 is supported.

### Dependencies

- [crablock](https://codeberg.org/crabjail/crablock) >= 0.1.0
  - libseccomp
  - pasta
  - xdg-dbus-proxy

### Prerequisites

- [Rust](https://www.rust-lang.org/) >= 1.85.0
- [meson](https://mesonbuild.com/) >= 0.62
- [scdoc](https://git.sr.ht/~sircmpwn/scdoc)
- [mdbook](https://rust-lang.github.io/mdBook/)

**Fedora Linux:**

    dnf install cargo meson scdoc

### Build

    meson setup _builddir --buildtype=release
    meson compile -C _builddir

### Install

    export MESON_ROOT_CMD=run0
    meson install -C _builddir --no-rebuild

## Usage

TBW

## Examples

TBW

## Related projects

<sub>In alphabetical order:</sub>

- [bubblejail](https://github.com/igo95862/bubblejail)
- [firejail](https://firejail.wordpress.com/)
- [minijail](https://google.github.io/minijail/)
- [nsjail](https://nsjail.dev/)

<sub>Others:</sub>

- [bubblewrap](https://github.com/containers/bubblewrap)
- [chroot](https://man7.org/linux/man-pages/man1/chroot.1.html)
- Containers
  - [crun](https://github.com/containers/crun)
  - [docker](https://www.docker.com/)
  - [lxc](https://linuxcontainers.org/lxc/)
  - [lxd](https://linuxcontainers.org/lxd/)
  - [podman](https://podman.io/)
  - [runc](https://github.com/opencontainers/runc)
  - runsc aka. gVisor
  - [systemd-nspawn](https://www.freedesktop.org/software/systemd/man/systemd-nspawn.html)
- [flatpak](https://flatpak.org/)
- Kernels:
  - [gVisor](https://gvisor.dev/)
  - [seitan](https://seitan.rocks/)
  - [sydbox](https://sydbox.exherbo.org)
- [User Mode Linux](https://docs.kernel.org/virt/uml/user_mode_linux_howto_v2.html)
- Linux Security Modules (LSM)
  - Mandatory Access Control (MAC)
    - [AppArmor](https://www.kernel.org/doc/html/latest/admin-guide/LSM/apparmor.html)
    - [SELinux](https://www.kernel.org/doc/html/latest/admin-guide/LSM/SELinux.html)
    - SMACK
    - [TOMOYO](https://www.kernel.org/doc/html/latest/admin-guide/LSM/tomoyo.html)
  - [Landlock](https://www.kernel.org/doc/html/latest/userspace-api/landlock.html)
- [Sandboxed API](https://developers.google.cn/sandboxed-api)
- Virtual Machines

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md)

## Credits / Acknowledgments

## FAQ

## Links

Homepage: https://crabjail.codeberg.page/

Repository: https://codeberg.org/crabjail/crabjail

Bug-Tracker: https://codeberg.org/crabjail/issues

## Changelog

See [CHANGELOG.md](CHANGELOG.md) for the full changelog.

## Authors and Contributors

See [AUTHORS](AUTHORS)

## License

GPL-3.0-or-later, see [COPYING](COPYING) for details.

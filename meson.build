project('crabjail',
  # Keep in sync with Cargo.toml
  version: '0.1.0',
  license: 'GPL-3.0-or-later',
  meson_version: '>=0.62',
  default_options: [
    'strip=true',
  ],
)

fs = import('fs')

# https://github.com/mesonbuild/meson/issues/825
docdir = get_option('datadir') / 'doc' / meson.project_name()

assert(
  get_option('buildtype') in ['debug', 'release'],
  'crabjail cannot be built with -Dbuildtype=@0@. Only "debug" and "release" are supported.'.format(get_option('buildtype')),
)

subdir('docs')
subdir('man')

install_data('README.md', 'CHANGELOG.md', 'COPYING', 'AUTHORS',
  install_dir: docdir,
)

# -- Begin common Rust --

find_program('cargo', version: '>=1.85')
cargo = find_program(meson.project_source_root() / 'build-aux/cargo')

common_cargo_args = []
if get_option('buildtype') == 'release'
  common_cargo_args += '--release'
endif

# -- End Common Rust --

# -- Begin cjail --

cjail_cargo_env = environment()

cjail_cargo_args = common_cargo_args
cjail_cargo_args += ['--manifest-path', files('Cargo.toml')]

cjail_sources = [
  'Cargo.lock',
  'Cargo.toml',
  # find src -type f | sed "s/.*/  '\0',/g" | LC_ALL=C sort
  'src/app_id.rs',
  'src/bin/cjail/cli.rs',
  'src/bin/cjail/main.rs',
  'src/config.rs',
  'src/config/syscall_groups.rs',
  'src/crablock_cmd.rs',
  'src/dirs.rs',
  'src/dynamic_permissions.rs',
  'src/dynamic_permissions/move_to_rhai.rs',
  'src/error.rs',
  'src/expand_path.rs',
  'src/jail.rs',
  'src/jail/clock_arg_macros.rs',
  'src/jail/dbus.rs',
  'src/jail/filesystem.rs',
  'src/jail/namespaces_limits.rs',
  'src/jail/network.rs',
  'src/jail/syscall_filter.rs',
  'src/jail_file.rs',
  'src/jail_file/toml_model.rs',
  'src/lib.rs',
  'src/permissions.rs',
  'src/utils.rs',
]

custom_target('cjail',
  build_by_default: true,
  console: true,
  command: [
    cargo,
    '--build-root', '@BUILD_ROOT@',
    '--output', '@OUTPUT@',
    '--buildtype', get_option('buildtype'),
    '--',
    'build', cjail_cargo_args,
  ],
  env: cjail_cargo_env,
  input: cjail_sources,
  output: 'cjail',
  install: true,
  install_dir: get_option('bindir'),
  install_mode: 'r-xr-xr-x',
)

# -- End cjail --

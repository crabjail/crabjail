Crabjail Documentation
======================

To preview it, install [mdbook](https://rust-lang.github.io/mdBook/) and run

```bash
mdbook serve -on 127.0.0.1 docs
```
